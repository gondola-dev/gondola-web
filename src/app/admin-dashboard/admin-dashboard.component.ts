import { Component, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { GondolaService } from 'src/app/services/gondola.service';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  @Output() sideNavToggled = new EventEmitter<boolean>();
  menuStatus: boolean = false;
  isCollapsed = false;
  
  constructor(public gondolaService: GondolaService,
    private router: Router) { }

  ngOnInit(): void {
  }
  openUsersList() {
    this.router.navigate(['/BetaUsersList']);
}

SideNavToggle(){
  this.menuStatus = !this.menuStatus;
  this.sideNavToggled.emit(this.menuStatus);
}


}