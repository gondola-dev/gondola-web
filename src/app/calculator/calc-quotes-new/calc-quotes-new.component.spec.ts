import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcQuotesNewComponent } from './calc-quotes-new.component';

describe('CalcQuotesNewComponent', () => {
  let component: CalcQuotesNewComponent;
  let fixture: ComponentFixture<CalcQuotesNewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcQuotesNewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcQuotesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
