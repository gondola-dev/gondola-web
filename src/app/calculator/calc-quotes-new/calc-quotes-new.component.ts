import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { CalcQuotesComponent } from '../calc-quotes/calc-quotes.component';
import { GondolaService } from 'src/app/services/gondola.service'; 
import { ProceedsLastComponent } from '../proceeds-last/proceeds-last.component';
import { NeedvswantComponent } from '../needvswant/needvswant.component';
import { LoginModelComponent } from 'src/app/home-section/login-model/login-model.component';
import { MinimumCoverageComponent } from '../minimum-coverage/minimum-coverage.component';

@Component({
  selector: 'app-calc-quotes-new',
  templateUrl: './calc-quotes-new.component.html',
  styleUrls: ['./calc-quotes-new.component.scss']
})
export class CalcQuotesNewComponent implements OnInit {
  isCollapsed = false;
  

  constructor(private router: Router,
    public gondolaService: GondolaService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.gondolaService.IsThisUs = false;
    this.gondolaService.IsInvite = false;
    this.gondolaService.isHome = false;
    this.gondolaService.IsCalc = true;
    window.scroll(0, 0);
    
  }
  openCalcPop() {
    const dialogRef = this.dialog.open(CalcQuotesComponent, {
      data: {
        message: 'data',
      },
    });

  }
  backToHome(){
    
  }
  openProceedsPop(){
    const dialogRef = this.dialog.open(ProceedsLastComponent, {
      data: {
        message: 'data',
      },
    });
  }
  openNeedvsWant(){
    const dialogRef = this.dialog.open(NeedvswantComponent, {
      data: {
        message: 'data',
      },
    });
  }
  navigateToThisIsUs(){
    this.router.navigate(['/ThisIsUs']);
  }
  navigateToHome(el ?: any){
    this.gondolaService.age = '';
    this.gondolaService.retirementAge ='';
    this.gondolaService.income = '';
    this.gondolaService.amount = '';
    if(el){
      this.gondolaService.navPageInfo = el;
    }
    this.router.navigate(['']);
  }

  navigateToInvite(){
    this.router.navigate(['/BetaInvite']);
  }

  openMinimumCoveragePop(){
    const dialogRef = this.dialog.open(MinimumCoverageComponent, {
      data: {
        message: 'data',
      },
    });
  }

}
