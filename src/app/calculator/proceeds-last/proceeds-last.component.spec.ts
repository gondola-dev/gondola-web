import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProceedsLastComponent } from './proceeds-last.component';

describe('ProceedsLastComponent', () => {
  let component: ProceedsLastComponent;
  let fixture: ComponentFixture<ProceedsLastComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProceedsLastComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProceedsLastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
