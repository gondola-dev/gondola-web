import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalcQuotesComponent } from '../calc-quotes/calc-quotes.component';
import { GondolaService } from 'src/app/services/gondola.service';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';

@Component({
  selector: 'app-proceeds-last',
  templateUrl: './proceeds-last.component.html',
  styleUrls: ['./proceeds-last.component.scss']
})
export class ProceedsLastComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
  private dialogRef: MatDialogRef<CalcQuotesComponent>,
  private gondolaService: GondolaService,
  private formBuilder: FormBuilder,
  private router: Router,) {dialogRef.disableClose = true; }
  iscalculated = false;
  isSubmitDone = false;
  amount = '';
  savings = '';
  invreturn = '4';
  income = '';
  inflationrate = '3.26';
  year = '';
  /**
   *  quotes details from group
   */
  proceedLastForm!: FormGroup;

  ngOnInit(): void {
    if(this.gondolaService.income){
      this.income = '$'.concat(this.numberWithCommas(this.gondolaService.income));
    }
    if(this.gondolaService.amount){
      this.amount = '$'.concat(this.numberWithCommas(this.gondolaService.amount));
    }
    
    this.proceedLastForm = this.formBuilder.group({
      amount: new FormControl(this.amount, Validators.required),
      savings: new FormControl(this.savings, Validators.required),
      invreturn: new FormControl(this.invreturn,  Validators.required),
      income: new FormControl(this.income,  Validators.required),
      inflationrate: new FormControl(this.inflationrate,  Validators.required),
    });
  }
  closeDialog(){
    this.dialogRef.close();
  }

  calcQuotes(){
    this.isSubmitDone = true;
    if(this.proceedLastForm.valid){
      let income = Number(this.removeIncome(this.income));
      let savings = Number(this.removeIncome(this.savings));
      let amount = Number(this.removeIncome(this.amount));
      let balancebfrWithdraw = savings + amount;
      let balanceaftrWithdraw;
      let annualWithdrawl = income;
      let annualInterestRate;
      let balanceEnd;
      for(let i = 1 ; balancebfrWithdraw > 0 ; i++){
        console.log('balancebfrWithdraw  -- ',balancebfrWithdraw);
        balanceaftrWithdraw = balancebfrWithdraw - annualWithdrawl;
        console.log('balanceaftrWithdraw  -- ',balanceaftrWithdraw);
        annualInterestRate = balanceaftrWithdraw * (Number(this.invreturn)/100);
        console.log('annualInterestRate  -- ',annualInterestRate);
        balanceEnd = (balancebfrWithdraw - annualWithdrawl) + annualInterestRate;
        balancebfrWithdraw = (balancebfrWithdraw + annualInterestRate) - annualWithdrawl;
        console.log('balancebfrWithdraw  -- ',balancebfrWithdraw);
        annualWithdrawl = Number((annualWithdrawl * (1 + (Number(this.inflationrate)/100))).toFixed(2));
        console.log('annualWithdrawl  -- ',annualWithdrawl);
        if(balancebfrWithdraw > 0){
          this.year = i.toString();
          console.log('this.year  -- ',this.year);
        }
      }
      this.iscalculated = true;
    }
  }

  
  convertAmount(event: any) {
    this.amount = event.target.value;
    this.amount = this.removeDollar(this.amount);
    this.proceedLastForm.get('amount')?.setValue(this.amount);
    if (this.amount !== null && this.amount !== '' && this.amount !== undefined && this.isNumeric(this.amount)) {
      this.amount = this.numberWithCommas(this.amount.replace(/,/g, ''));
      this.amount = '$'.concat(this.amount);
      this.proceedLastForm.get('amount')?.setValue(this.amount);
    }
  }
  
  onFocusAmount(e: any) {
    this.amount = e.target.value;
  }
  numberWithCommas(e: any) {
    let number = e;
    if(number && null !== number){
      number = number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return number;
  }
  onFocusIncome(e: any) {
    this.income = e.target.value;
  }
  
  convertIncome(event: any) {
    this.income = event.target.value;
    this.income = this.removeDollar(this.income);
    this.proceedLastForm.get('income')?.setValue(this.income);
    if (this.income !== null && this.income !== '' && this.income !== undefined && this.isNumeric(this.income)) {
      this.income = this.numberWithCommas(this.income.replace(/,/g, ''));
      this.income = '$'.concat(this.income);
      this.proceedLastForm.get('income')?.setValue(this.income);
    }
  }
  onFocusSavings(e: any) {
    this.savings = e.target.value;
  }
  
  convertSavings(event: any) {
    this.savings = event.target.value;
    this.savings = this.removeDollar(this.savings);
    this.proceedLastForm.get('savings')?.setValue(this.savings);
    if (this.savings !== null && this.savings !== '' && this.savings !== undefined && this.isNumeric(this.savings)) {
      this.savings = this.numberWithCommas(this.savings.replace(/,/g, ''));
      this.savings = '$'.concat(this.savings);
      this.proceedLastForm.get('savings')?.setValue(this.savings);
    }
  }
  removeDollar(amount: string){
    return amount ? amount.toString().replace(/\$/g, '') : '';
  }
  isNumeric(e: any): boolean {
    if (/^(\d+)?([.]?\d{0,9})?$/.test(this.removeCommas(e))) return true;
    else return false;
  }
  removeCommas(e: any) {
    return e.toString().replace(/,/g, '');
  }
  removeIncome(amount: string): string {
    let amt = '';
    if (amount && null !== amount) {
      amt = amount.replace(/\$/g, '');
      amt = this.removeCommas(amt);
    }
    return amt;
  }

}
