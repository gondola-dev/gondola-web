import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-calc-document-proof',
  templateUrl: './calc-document-proof.component.html',
  styleUrls: ['./calc-document-proof.component.scss']
})
export class CalcDocumentProofComponent implements OnInit {
  calcValuesList:any = [];
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
  private dialogRef: MatDialogRef<CalcDocumentProofComponent>) {dialogRef.disableClose = true; }

  ngOnInit(): void {
    this.calcValuesList = this.data.calcValuesList;
    console.log('calcValuesList ',this.calcValuesList);
  }

  
  closeDialog(){
    this.dialogRef.close();
  }

}
