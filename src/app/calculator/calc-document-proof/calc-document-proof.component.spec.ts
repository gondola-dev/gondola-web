import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcDocumentProofComponent } from './calc-document-proof.component';

describe('CalcDocumentProofComponent', () => {
  let component: CalcDocumentProofComponent;
  let fixture: ComponentFixture<CalcDocumentProofComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcDocumentProofComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcDocumentProofComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
