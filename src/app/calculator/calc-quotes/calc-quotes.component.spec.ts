import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcQuotesComponent } from './calc-quotes.component';

describe('CalcQuotesComponent', () => {
  let component: CalcQuotesComponent;
  let fixture: ComponentFixture<CalcQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalcQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
