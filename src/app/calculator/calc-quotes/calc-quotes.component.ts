import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GondolaService } from 'src/app/services/gondola.service';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { CalcDocumentProofComponent } from '../calc-document-proof/calc-document-proof.component';

@Component({
  selector: 'app-calc-quotes',
  templateUrl: './calc-quotes.component.html',
  styleUrls: ['./calc-quotes.component.scss']
})
export class CalcQuotesComponent implements OnInit {
  isSubmitDone = false;
  incomeGrowth = '3.9';
  afterTaxReturn = '4';
  fringeBenefits = '39';
  serviceFamily = '20';
  selfMaintainance = '12';
  income = '';
  age = '';
  retirementage = '';

  iscalculated = false;
  /**
   *  quotes details from group
   */
  calcDetailsForm!: FormGroup;

  cummulativeValue = '';
  /*
  * To store the value of
  */
  calcValues: HumanLifeCal[] = [];


  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<CalcQuotesComponent>,
    private gondolaService: GondolaService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog,
    private router: Router) {
    dialogRef.disableClose = true;
  }
  ngOnInit(): void {
    console.log(this.gondolaService);
    this.age = this.gondolaService.age;
    this.retirementage = this.gondolaService.retirementAge;
    if (this.gondolaService.income) {
      this.income = '$'.concat(this.numberWithCommas(this.gondolaService.income));
    }

    this.calcDetailsForm = this.formBuilder.group({
      age: new FormControl(this.age, Validators.required),
      retirementage: new FormControl(this.retirementage,Validators.required),
      income: new FormControl(this.income,Validators.required),
      incomeGrowth: new FormControl(this.incomeGrowth,Validators.required),
      fringeBenefits: new FormControl(this.fringeBenefits,Validators.required),
      selfMaintainance: new FormControl(this.selfMaintainance,Validators.required),
      serviceFamily: new FormControl(this.serviceFamily,Validators.required),
      afterTaxReturn: new FormControl(this.afterTaxReturn,Validators.required),
    });
  }

  closeDialog() {
    this.dialogRef.close();
  }
  submitQuotes() {
    this.isSubmitDone = true;
  }

  calcQuotes() {
    this.calcValues= [];
    let cummulativeVal = 0;
    let age = Number(this.age);
    let income = Number(this.removeIncome(this.income)).toFixed(2);
    const totalCount = (Number(this.retirementage) - Number(this.age)) + 1;
    for (let i = 0; i < totalCount; i++) {
      const hlc = new HumanLifeCal();
      hlc.age = age++;
      hlc.year = i+1;
      if (i != 0) {
        income = (Number(income) * (1 + (Number(this.incomeGrowth) / 100))).toFixed(2);
      }
      hlc.income = income;
      const personalMaintance = (Number(income) * (Number(this.selfMaintainance) / 100)).toFixed(2);
      hlc.personalMaintance = this.numberWithCommas(personalMaintance);;
      const fringeBenefits = (Number(income) * (Number(this.fringeBenefits) / 100)).toFixed(2);
      hlc.fringeBenefits = this.numberWithCommas(fringeBenefits);;
      const serviceReplacement = (Number(income) * (Number(this.serviceFamily) / 100)).toFixed(2);
      hlc.serviceReplacement = this.numberWithCommas(serviceReplacement);;
      const annualSupport = ((Number(income) + Number(fringeBenefits) + Number(serviceReplacement)) - Number(personalMaintance)).toFixed(2);
      hlc.annualSupport = this.numberWithCommas(annualSupport);;
      const presentVal = i === 0 ? annualSupport : (Number(annualSupport) / Math.pow(1 + (Number(this.afterTaxReturn) / 100), i + 1)).toFixed(2);
      hlc.presentVal = this.numberWithCommas(presentVal);
      cummulativeVal = i === 0 ? Number(presentVal) : cummulativeVal + Number(presentVal);
      hlc.cummulativeVal = this.numberWithCommas(cummulativeVal.toFixed(2));
      this.calcValues.push(hlc);
    }
    if(cummulativeVal > 0){
      this.iscalculated = true;
    }
    this.cummulativeValue = this.numberWithCommas(cummulativeVal.toFixed(2));
  }

  numberWithCommas(e: any) {
    let number = e;
    if (number && null !== number) {
      number = number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return number;
  }
  onFocusIncome(e: any) {
    this.income = e.target.value;
  }

  convertIncome(event: any) {
    this.income = event.target.value;
    this.income = this.removeDollar(this.income);
    this.calcDetailsForm.get('income')?.setValue(this.income);
    if (this.income !== null && this.income !== '' && this.income !== undefined && this.isNumeric(this.income)) {
      this.income = this.numberWithCommas(this.income.replace(/,/g, ''));
      this.income = '$'.concat(this.income);
      this.calcDetailsForm.get('income')?.setValue(this.income);
    }
  }
  removeDollar(amount: string) {
    return amount ? amount.toString().replace(/\$/g, '') : '';
  }
  isNumeric(e: any): boolean {
    if (/^(\d+)?([.]?\d{0,9})?$/.test(this.removeCommas(e))) return true;
    else return false;
  }
  removeCommas(e: any) {
    return e.toString().replace(/,/g, '');
  }
  removeIncome(amount: string): string {
    let amt = '';
    if (amount && null !== amount) {
      amt = amount.replace(/\$/g, '');
      amt = this.removeCommas(amt);
    }
    return amt;
  }

  removedPlansModal(): void {
    const dialogRef = this.dialog.open(CalcDocumentProofComponent, {
      data: {
        calcValuesList: this.calcValues,
      },
    });
  }
}


class HumanLifeCal{
  /*
  * variable
  */
  year = 0;
  /*
  * variable
  */
  age = 0;
  /*
  * variable
  */
  income = '';
  /*
  * variable
  */
  personalMaintance = '';
  /*
  * variable
  */
  fringeBenefits = '';
  /*
  * variable
  */
  serviceReplacement = '';
  /*
  * variable
  */
  annualSupport = '';
  /*
  * variable
  */
  presentVal = '';
  /*
  * variable
  */
  cummulativeVal = '';
}