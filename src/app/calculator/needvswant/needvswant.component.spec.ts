import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NeedvswantComponent } from './needvswant.component';

describe('NeedvswantComponent', () => {
  let component: NeedvswantComponent;
  let fixture: ComponentFixture<NeedvswantComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NeedvswantComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NeedvswantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
