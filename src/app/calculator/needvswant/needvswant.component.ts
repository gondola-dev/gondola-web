import { Component, Inject, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSliderChange } from '@angular/material/slider';
import { Router } from '@angular/router';
import { CalcQuotesComponent } from '../calc-quotes/calc-quotes.component';

@Component({
  selector: 'app-needvswant',
  templateUrl: './needvswant.component.html',
  styleUrls: ['./needvswant.component.scss']
})
export class NeedvswantComponent implements OnInit {


  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    private dialogRef: MatDialogRef<CalcQuotesComponent>,
    private router: Router,) { dialogRef.disableClose = true; }
  iscalculated = false;
  value = 1;
  value2 = 0;
  new = 0;
  sliderControl: FormControl = new FormControl(0);

  ngOnInit(): void {
  }


  closeDialog() {
    this.dialogRef.close();
  }
  getSliderValue(event: any) {
    console.log(event.value);

    this.value2 = event.value / 100.0;
    this.value = 1.0 - event.value / 100.0;
    console.log(this.value, this.value2)
    if (this.value2 === 0) {
      this.value2 = 0.2;
      // console.log(this.value2)
    }
    if (event.value === 100) {
      this.value = 0.2;
      // console.log(this.value)
    } else if (event.value === 0) {
      this.value = 1;
    }
    document.documentElement.style.setProperty('--opacityImg1', this.value.toString());
    document.documentElement.style.setProperty('--opacityImg2', this.value2.toString())
  }
  getSliderValue1(event: any) {
    console.log(event);
    this.value2 = event.value / 100.0;
    this.value = 1.0 - event.value / 100.0;
    if (this.value2 === 0) {
      this.value2 = 0.2;
      console.log(this.value2)
    }
    if (event.value === 100) {
      this.value = 0.2;
      console.log(this.value)
    } else if (event.value === 0) {
      this.value = 1;
    }
    document.documentElement.style.setProperty('--opacityImg3', this.value.toString());
    document.documentElement.style.setProperty('--opacityImg4', this.value2.toString())
  }
  getSliderValue2(event: any) {
    console.log(event);
    this.value2 = event.value / 100.0;
    this.value = 1.0 - event.value / 100.0;
    if (this.value2 === 0) {
      this.value2 = 0.2;
      console.log(this.value2)
    }
    if (event.value === 100) {
      this.value = 0.2;
      console.log(this.value)
    } else if (event.value === 0) {
      this.value = 1;
    }

    document.documentElement.style.setProperty('--opacityImg5', this.value.toString());
    document.documentElement.style.setProperty('--opacityImg6', this.value2.toString())
  }
  formatLabel(value: number) {
    if (value >= 1000) {
      return Math.round(value / 1000) + 'k';
    }

    return value;
    console.log(value)
  }
  resetForm(): void {
    this.sliderControl.reset(0);
  }

}
