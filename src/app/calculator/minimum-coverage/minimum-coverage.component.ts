import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CalcQuotesComponent } from '../calc-quotes/calc-quotes.component';
import { GondolaService } from 'src/app/services/gondola.service';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';



@Component({
  selector: 'app-minimum-coverage',
  templateUrl: './minimum-coverage.component.html',
  styleUrls: ['./minimum-coverage.component.scss']
})
export class MinimumCoverageComponent implements OnInit {

  income = '';
  savings = '';
  invreturn = '4';
  minimumCoverage = '';
  isSubmitDone = false;
  /**
   *  quotes details from group
   */
  coverageDetailsForm!: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    private gondolaService: GondolaService,
    private dialogRef: MatDialogRef<CalcQuotesComponent>,
    private formBuilder: FormBuilder,
    private router: Router,) { dialogRef.disableClose = true; }
  iscalculated = false;

  ngOnInit(): void {
    if (this.gondolaService.income) {
      this.income = '$'.concat(this.numberWithCommas(this.gondolaService.income));
    }

    this.coverageDetailsForm = this.formBuilder.group({
      income: new FormControl(this.income, Validators.required),
      savings: new FormControl(this.savings, Validators.required),
      invreturn: new FormControl(this.invreturn, Validators.required),
    });
  }
  closeDialog() {
    this.dialogRef.close();
  }

  calcCoverage() {
    this.isSubmitDone = true;
    if (this.coverageDetailsForm.valid) {
      let income = Number(this.removeIncome(this.income));
      let savings = Number(this.removeIncome(this.savings));
      const minCoverage = (income / (Number(this.invreturn) / 100)) - savings + income;
      this.minimumCoverage = this.numberWithCommas(minCoverage.toFixed(2));
      this.iscalculated = true;
    }
  }

  numberWithCommas(e: any) {
    let number = e;
    if (number && null !== number) {
      number = number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return number;
  }
  onFocusIncome(e: any) {
    this.income = e.target.value;
  }

  convertIncome(event: any) {
    this.income = event.target.value;
    this.income = this.removeDollar(this.income);
    this.coverageDetailsForm.get('income')?.setValue(this.income);
    if (this.income !== null && this.income !== '' && this.income !== undefined && this.isNumeric(this.income)) {
      this.income = this.numberWithCommas(this.income.replace(/,/g, ''));
      this.income = '$'.concat(this.income);
      this.coverageDetailsForm.get('income')?.setValue(this.income);
    }
  }
  onFocusSavings(e: any) {
    this.savings = e.target.value;
  }

  convertSavings(event: any) {
    this.savings = event.target.value;
    this.savings = this.removeDollar(this.savings);
    this.coverageDetailsForm.get('savings')?.setValue(this.savings);
    if (this.savings !== null && this.savings !== '' && this.savings !== undefined && this.isNumeric(this.savings)) {
      this.savings = this.numberWithCommas(this.savings.replace(/,/g, ''));
      this.savings = '$'.concat(this.savings);
      this.coverageDetailsForm.get('savings')?.setValue(this.savings);
    }
  }
  removeDollar(amount: string) {
    return amount ? amount.toString().replace(/\$/g, '') : '';
  }
  isNumeric(e: any): boolean {
    if (/^(\d+)?([.]?\d{0,9})?$/.test(this.removeCommas(e))) return true;
    else return false;
  }
  removeCommas(e: any) {
    return e.toString().replace(/,/g, '');
  }
  removeIncome(amount: string): string {
    let amt = '';
    if (amount && null !== amount) {
      amt = amount.replace(/\$/g, '');
      amt = this.removeCommas(amt);
    }
    return amt;
  }
}
