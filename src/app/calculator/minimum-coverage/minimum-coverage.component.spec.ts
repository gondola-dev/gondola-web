import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinimumCoverageComponent } from './minimum-coverage.component';

describe('MinimumCoverageComponent', () => {
  let component: MinimumCoverageComponent;
  let fixture: ComponentFixture<MinimumCoverageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinimumCoverageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinimumCoverageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
