import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RemovedPlansComponent } from './removed-plans.component';

describe('RemovedPlansComponent', () => {
  let component: RemovedPlansComponent;
  let fixture: ComponentFixture<RemovedPlansComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RemovedPlansComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemovedPlansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
