import { Component,Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-removed-plans',
  templateUrl: './removed-plans.component.html',
  styleUrls: ['./removed-plans.component.scss']
})
export class RemovedPlansComponent implements OnInit {
  

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
  private dialogRef: MatDialogRef<RemovedPlansComponent>) {dialogRef.disableClose = true; }

  ngOnInit(): void {
  }
  closeDialog(){
    this.dialogRef.close();
  }
}
