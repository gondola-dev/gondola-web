import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-print-quotes',
  templateUrl: './print-quotes.component.html',
  styleUrls: ['./print-quotes.component.scss']
})
export class PrintQuotesComponent implements OnInit {
  /*
  * To store the value of
  */
  amount = '';
  pageView = 1;
  pageName = 'quotes';
  /*
  * To store the value of
  */
  years = '';
  /*
  * To store the value of
  */
  estAnnualIncome = '';
  /*
  * To store the value of
  */
  estRetirementage = '';
  /*
  * To store the value of
  */
  estAssets = '';
  /*
  * To store the value of
  */
  isSmoker = '';
  /*
  * To store the value of
  */
  isWorking = '1';
  /*
  * To store the value of
  */
  age = '';
  /*
  * To store the value of
  */
  state = '';
  /*
  * To store the value of
  */
  statecode = '';
  /*
  * To store the value of
  */
  gender = '';
  /**
   *   filter array for protection 
  */
  addOnFilter: any[] = []

  /**
   * product_list 
   */
   master_product_list: any[] = [];
   /*
   * variable
   */
   productList: Product[] = [];
   /*
   * variable
   */
   totalNumberOfProducts = 0;

  constructor(
    public gondolaService: GondolaService,
    private SpinnerService: NgxSpinnerService,) { }

  ngOnInit(): void {
    if (this.gondolaService.requestData !== null) {
      let data = this.gondolaService.requestData;
      console.log(this.gondolaService.requestData);
      //this.gondolaService.requestData = null;
      this.age = data.age;
      this.gender = data.gender;
      this.state = data.state;
      this.statecode = data.statecode;
      this.isSmoker = data.issmoker;
      this.isWorking = data.isWorking;
      this.years = data.years;
      this.amount = data.amount;
      this.estAnnualIncome = data.annualincome;
      this.estRetirementage = data.expectedretirementage;
      this.estAssets = data.estAssets;
    }
    if (this.gondolaService.productList) {
      this.master_product_list = this.gondolaService.productList;
      this.gondolaService.productList = null;
      this.mapProductDetails(this.master_product_list);
    }
  }

  
  /*
  * function to map the product list
  */
  mapProductDetails(data: any) {
    this.productList = [];
    this.totalNumberOfProducts = data.numberOfProductsFiltered;
    const product_list = data.filteredProductList;
    if (this.totalNumberOfProducts > 0 && product_list !== null) {
      product_list.forEach((element: any) => {
        const product = new Product();
        product.productid = element.productid;
        product.productname = this.sliceChars(element.productname);
        product.productGuarenteedYears = element.productGuarenteedYears;
        product.productYearsLevel = element.productYearsLevel;
        product.productMinAgeIssued = element.productMinAgeIssued;
        product.productMaxAgeIssued = element.productMaxAgeIssued;
        product.productMinFaceAmount = element.productMinFaceAmount;
        product.productMaxFaceAmount = this.numberWithCommas(element.productMaxFaceAmount);
        product.productMaxYearsToShow = element.productMaxYearsToShow;
        product.productNAICCertified = element.productNAICCertified;
        product.companyName = element.companyName;
        product.companyNAICCertified = element.companyNAICCertified;
        product.companyNAICCode = element.companyNAICCode;
        product.companyRating = element.companyRating;
        product.guaranteedAnnualPremiumAmount = element.guaranteedAnnualPremiumAmount;
        product.guaranteedMonthlyPremiumAmount = element.guaranteedMonthlyPremiumAmount;
        product.guaranteedQuarterlyPremiumAmount = element.guaranteedQuarterlyPremiumAmount;
        product.guaranteedSemiAnnualPremiumAmount = element.guaranteedSemiAnnualPremiumAmount;
        product.healthRatingId = element.healthRatingId;
        product.healthRatingName = element.healthRatingName;
        product.speedToProtectionName = element.speedToProtectionname;
        product.companyRatingId = element.companyRatingId;
        product.waiveraddonsid = element.waiveraddonsid;
        product.waiveraddonsname = element.waiveraddonsname;
        product.adbaddonsid = element.adbaddonsid;
        product.adbaddonsname = element.adbaddonsname;
        product.childaddonsid = element.childaddonsid;
        product.childaddonsname = element.childaddonsname;
        product.speedToProtectionId = element.speedToProtectionid;
        product.isTopPick = element.topPick;
        product.allcompanyratings = element.allcompanyratings;
        product.companyAddress = element.companyAddress;
        product.companyfounded = element.companyfounded;
        product.companyphonenumber = element.companyphonenumber;
        product.companywebsite = element.companywebsite;
        product.conversionname = element.conversionname;
        product.isconversion = element.isconversion;
        product.totalpremiumvalues = element.totalpremiumvalues;
        product.faceAmount = element.faceAmount;
        product.underWritingClass = element.underWritingClass;
        product.underWritingClassId = element.underWritingClassId;
        product.tableRatingId = element.tableRatingId;
        this.productList.push(product);
      });
    }
  }
  sliceChars(name: string): string {
    let s: any = [];
    if (name) {
      s = name.split("(");
    }
    return s[0];
  }
  numberWithCommas(e: any) {
    let number = e;
    if(number && null !== number){
      number = number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return number;
  }

  getQuotes(event: any){
    this.SpinnerService.show()
    console.log(event);
    const isChecked = event.target.checked;
    console.log(isChecked);
    const QuoteObject = this.createNewQuoteObject(isChecked);
    console.log(QuoteObject);
    this.gondolaService.createQuotes(QuoteObject).subscribe(
      resObj => {
        if (resObj !== null) {
          this.mapProductDetails(resObj);
          this.SpinnerService.hide()
        }
      },
      error => {
        this.SpinnerService.hide()
      });
  }

  /*
  * function to create new request object
  */
  createNewQuoteObject(isChecked: boolean): any {
    let addonlist: any = [];
    const requestObject: any = {};
    requestObject.amount = this.amount;
    requestObject.years = this.years;
    requestObject.annualincome = this.estAnnualIncome;
    requestObject.expectedretirementage = this.estRetirementage;
    requestObject.issmoker = this.isSmoker;
    requestObject.isWorking = this.isWorking;
    requestObject.age = this.age;
    requestObject.state = this.state;
    requestObject.statecode = this.statecode;
    requestObject.gender = this.gender;
    requestObject.estAssets = this.estAssets;
    requestObject.ipaddress = this.gondolaService.ipAddress;
    requestObject.withNaic = isChecked ? '1' : '0';
    if (this.addOnFilter !== null && this.addOnFilter.length > 0) {
      this.addOnFilter.forEach((e) => {
        addonlist.push(e.type);
      })
    }
    if (addonlist && addonlist.length > 0) {
      requestObject.advancefilter = true;
      requestObject.addonslist = addonlist;
    }
    return requestObject;
  }

}



class Product {
  /*
  * variable
  */
  companyNAICCertified = '';
  /*
  * variable
  */
  companyNAICCode = '';
  /*
  * variable
  */
  companyName = '';
  /*
  * variable
  */
  companyRating = '';
  /*
  * variable
  */
  guaranteedAnnualPremiumAmount = '';
  /*
  * variable
  */
  guaranteedMonthlyPremiumAmount = '';
  /*
  * variable
  */
  guaranteedQuarterlyPremiumAmount = '';
  /*
  * variable
  */
  guaranteedSemiAnnualPremiumAmount = '';
  /*
  * variable
  */
  productGuarenteedYears = '';
  /*
  * variable
  */
  productYearsLevel = '';
  /*
  * variable
  */
  productMaxAgeIssued = '';
  /*
  * variable
  */
  productMaxFaceAmount = '';
  /*
  * variable
  */
  productMaxYearsToShow = '';
  /*
  * variable
  */
  productMinAgeIssued = '';
  /*
  * variable
  */
  productMinFaceAmount = '';
  /*
  * variable
  */
  productNAICCertified = false;
  /*
  * variable
  */
  productid = '';
  /*
  * variable
  */
  productname = '';
  /*
  * variable
  */
  healthRatingId: number = 0;
  /*
  * variable
  */
  healthRatingName = ''
  /*
  * variable
  */
  companyRatingId: number = 0;
  /*
   * variable
   */
  speedToProtectionId: number = 0;
  /*
  * variable
  */
  speedToProtectionName = '';
  /*
  * variable
  */
  waiveraddonsid: number  = 0;
  /*
  * variable
  */
	waiveraddonsname = '';
  /*
  * variable
  */
	adbaddonsid: number  = 0;
  /*
  * variable
  */
	adbaddonsname = '';
  /*
  * variable
  */
	childaddonsid: number  = 0;
  /*
  * variable
  */
	childaddonsname = '';
  /*
  * variable
  */
	isTopPick = false;
  /*
  * variable
  */
	allcompanyratings: any;
  /*
  * variable
  */
	companyfounded = '';
  /*
  * variable
  */
	companyphonenumber = '';
  /*
  * variable
  */
	companywebsite = '';
  /*
  * variable
  */
	conversionname = '';
  /*
  * variable
  */
  isconversion = false;
  /*
  * variable
  */
	companyAddress: any;
  /*
  * variable
  */
	totalpremiumvalues: any;
  /*
  * variable
  */
  faceAmount = '';

  underWritingClass = '';

  underWritingClassId = '';

  tableRatingId = '';
}

class Insurers{
  id: number = 0;
  insurername = '';
  insurershortname = '';
  naiccode = '';
  isAvailable = false;
  isChecked = false;
}