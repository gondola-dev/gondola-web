import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintQuotesComponent } from './print-quotes.component';

describe('PrintQuotesComponent', () => {
  let component: PrintQuotesComponent;
  let fixture: ComponentFixture<PrintQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
