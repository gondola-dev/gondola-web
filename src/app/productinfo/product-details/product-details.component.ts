import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {
  /*
  * To store the value of
  */
  amount = '';
  /*
  * To store the value of
  */
  years = '';
  /*
  * To store the value of
  */
  smoker = '';
  /*
  * To store the value of
  */
  age: number = 0;
  /*
  * To store the value of
  */
  statename = '';
  /*
  * To store the value of
  */
  gender = '';
  /*
  * To store the value of
  */
  productname = '';
  /*
  * To store the value of
  */
  companyName = '';
  /*
  * To store the value of
  */
  companyNAICCertified = '';
  /*
  * To store the value of
  */
  companyNAICCode = '';
  /*
  * To store the value of
  */
  companyRating = '';
  /*
  * To store the value of
  */
  premiumAmount = '';
  /*
  * To store the value of
  */
  paymentMode = '';

  /*
  * To store the value of
  */
  guaranteedMonthlyPremiumAmount = '';
  /*
  * To store the value of
  */
  guaranteedAnnualPremiumAmount = '';
  /*
  * To store the value of
  */
  healthRatingName = '';
  /*
  * To store the value of
  */
  speedToProtectionName = '';
  /*
  * To store the value of
  */
  addons = '';
  /*
  * To store the value of
  */
  allcompanyratings: any;
  /*
  * To store the value of
  */
  companyAddress: any;
  /*
  * To store the value of
  */
  companyfounded = '';
  /*
  * To store the value of
  */
  companyphonenumber = '';
  /*
  * To store the value of
  */
  companywebsite = '';
  /*
  * To store the value of
  */
  conversionname = '';
  /*
  * To store the value of
  */
  isconversion = false;
  /*
  * To store the value of
  */
  totalpremiumvalues: Premium[] = [];

  product: any;

  reqData: any;

  riderlist : any = [];

  isWaiver = false;

  isADB = false;

  isChild = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    public gondolaService: GondolaService,
    private dialogRef: MatDialogRef<ProductDetailsComponent>) { dialogRef.disableClose = true; }

  ngOnInit(): void {
    this.product = this.data.productInfo;
    this.reqData = this.data.reqObj;
    this.paymentMode = this.data.paymentMode;
    if (this.product && this.reqData) {
      this.mapDetailsInfo(this.product, this.reqData);
    }
  }

  mapDetailsInfo(productInfo: any, reqInfo: any) {
    if (reqInfo !== null) {
      this.amount = this.numberWithCommas(reqInfo.amount);
      this.years = reqInfo.years;
      this.smoker = reqInfo.issmoker === '1' ? 'No' : 'Yes';
      this.age = reqInfo.age;
      if (reqInfo.statecode !== null && this.gondolaService.States !== null) {
        this.gondolaService.States.forEach((e) => {
          if (e.code === reqInfo.statecode) {
            this.statename = e.name;
          }
        })
      }
      this.gender = reqInfo.gender;
      reqInfo.advancefilter = true;
      if(reqInfo.addonslist && reqInfo.addonslist.length > 0){
        reqInfo.addonslist.forEach((e: any)=>{
          if(e==='Waiver'){
            this.isWaiver = true;
          } else if(e === 'ADB'){
            this.isADB = true;
          } else if(e === ' Child'){
            this.isChild = true;
          }
        });
      }
    }
    if (productInfo !== null) {
      this.productname = productInfo.productname;
      this.companyName = productInfo.companyName;
      this.companyNAICCertified = productInfo.companyNAICCertified;
      this.companyNAICCode = productInfo.companyNAICCode;
      if(this.paymentMode === 'Monthly'){
        this.premiumAmount = productInfo.guaranteedMonthlyPremiumAmount;
      } else if(this.paymentMode === 'Quarterly'){
        this.premiumAmount = productInfo.guaranteedQuarterlyPremiumAmount;
      } else if(this.paymentMode === 'SemiAnnual'){
        this.premiumAmount = productInfo.guaranteedSemiAnnualPremiumAmount;
        this.paymentMode = 'Semi-Annual';
      } else if(this.paymentMode === 'Annual'){
        this.premiumAmount = productInfo.guaranteedAnnualPremiumAmount;
      }
      /*this.guaranteedAnnualPremiumAmount = productInfo.guaranteedAnnualPremiumAmount;
      this.guaranteedMonthlyPremiumAmount = productInfo.guaranteedMonthlyPremiumAmount;
      this.guaranteedQuarterlyPremiumAmount = productInfo.guaranteedQuarterlyPremiumAmount;
      this.guaranteedSemiAnnualPremiumAmount = productInfo.guaranteedSemiAnnualPremiumAmount;*/
      this.healthRatingName = productInfo.healthRatingName;
      this.speedToProtectionName = productInfo.speedToProtectionname;
      if(productInfo.waiveraddonsname !== null && this.isWaiver){
        this.riderlist.push('Waiver')
      }
      if(productInfo.adbaddonsname !== null && this.isADB){
        this.riderlist.push('ADB')
      }
      if(productInfo.childaddonsname !== null && this.isChild){
        this.riderlist.push('Child')
      }
      console.log('riders -- ',this.riderlist); 
      if(this.riderlist.length === 0){
        this.riderlist.push("No Add-Ons Selected");
      }
      /*this.waiveraddonsid = productInfo.waiveraddonsid;
      this.waiveraddonsname = productInfo.waiveraddonsname;
      this.adbaddonsid = productInfo.adbaddonsid;
      this.adbaddonsname = productInfo.adbaddonsname;
      this.childaddonsid = productInfo.childaddonsid;
      this.childaddonsname = productInfo.childaddonsname;*/
      /*if (null !== productInfo.allcompanyratings) {
        const rt = productInfo.allcompanyratings;
        if (null !== rt.ambest) {
          this.allcompanyratings = this.sliceChars(rt.ambest) + ' (AMBest), ';
        } if (null !== rt.comdex) {
          this.allcompanyratings += this.sliceChars(rt.comdex) + ' (Comdex), ';
        } if (null !== rt.fitch) {
          this.allcompanyratings += this.sliceChars(rt.fitch) + ' (Fitch), ';
        } if (null !== rt.moodys) {
          this.allcompanyratings += this.sliceChars(rt.moodys) + ' (Moodys), ';
        } if (null !== rt.sandP) {
          this.allcompanyratings += this.sliceChars(rt.sandP) + ' (SandP), ';
        }
      }*/
      this.allcompanyratings = productInfo.allcompanyratings;
      if (null !== productInfo.companyAddress) {
        this.companyAddress = productInfo.companyAddress;
        //this.companyAddress = productInfo.companyAddress.street + ',' + productInfo.companyAddress.cityStateZip;
      }
      this.companyfounded = productInfo.companyfounded;
      this.companyphonenumber = '+1 (' + productInfo.companyphonenumber + ')';
      this.companywebsite = productInfo.companywebsite;
      this.isconversion = productInfo.isconversion;
      this.conversionname = this.isconversion ? productInfo.conversionname : 'This Life Insurance policy is not convertible to permanent products or any other policy or policy type.';
      //this.totalpremiumvalues = productInfo.totalpremiumvalues;
      if (null !== productInfo.totalpremiumvalues) {
        const totVal = productInfo.totalpremiumvalues;
        let age = this.age;
        totVal.forEach((element: any) => {
          const pm = new Premium();
          pm.year = element.year;
          pm.premium = element.value;
          pm.age = age++;
          pm.amount = this.amount;
          this.totalpremiumvalues.push(pm);
        });
      }
    }
  }

  closeDialog() {
    this.dialogRef.close();
  }

  sliceChars(name: string): string {
    let s: any = [];
    if (name) {
      s = name.split("(");
    }
    return s[0];
  }

  //Print Page
  printThisPage(event: any) {
    event.preventDefault();
    let printContents, popupWin;
    const WindowPrt = window.open('', '', 'top=0,left=0,height=100%,width=auto');
    WindowPrt!.document.write(document.getElementById('modal-body-content')!.innerHTML);
    WindowPrt!.document.title = 'Product_Details';
    WindowPrt!.document.close();
    WindowPrt!.focus();
    WindowPrt!.print();
    WindowPrt!.close();
  }


  numberWithCommas(e: any) {
    return e ? e.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',') : '';
  }

  checkCR(rt: any): boolean {
    if (null !== rt && '' !== rt) {
      const rating = rt.trim().replaceAll('\n', '');
      if ('' !== rating && rating) {
        return true;
      } return false;
    }
    return false;
  }

}


class Premium {
  year = '';
  age = 0;
  premium = '';
  amount = '';
}
