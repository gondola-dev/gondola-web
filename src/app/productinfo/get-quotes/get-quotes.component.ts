import { Component, ElementRef, HostListener, OnInit, Renderer2, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { GondolaService } from 'src/app/services/gondola.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { CurrencyPipe } from '@angular/common';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { ProductDetailsComponent } from '../product-details/product-details.component';
import { map, Observable, startWith, shareReplay, switchMap } from 'rxjs';
import { RemovedPlansComponent } from '../removed-plans/removed-plans.component';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { ErrorPopupComponent } from 'src/app/popups/error-popup/error-popup.component';
import { LoginModelComponent } from 'src/app/home-section/login-model/login-model.component';
import { BetaLaunchComponent } from 'src/app/home-section/beta-launch/beta-launch.component';
import { AddonsPopupComponent } from 'src/app/popups/addons-popup/addons-popup.component';



@Component({

  selector: 'app-get-quotes',
  templateUrl: './get-quotes.component.html',
  styleUrls: ['./get-quotes.component.scss']
})

export class GetQuotesComponent implements OnInit {
  /**
   * This is the toogle button elemenbt, look at HTML and see its defination
   */
  @ViewChild('sortingToggleButton') sortingToggleButton!: ElementRef;
  @ViewChild('sortingMenu') sortingMenu!: ElementRef;
  @ViewChild('filterscroller') filterscroller!: ElementRef;

  @ViewChild('tankIdentifier') tankIdentifier!: ElementRef;
  @ViewChild('secondBoxIdentifier') secondBoxIdentifier!: ElementRef;
  /*
  * To submit the form to stored in api
  */
  isSubmitDone = false;
  /*
  * To store the value of
  */
  amount = '';
  pageView = 1;
  pageName = 'quotes';
  /*
  * To store the value of
  */
  years = '';
  /*
  * To store the value of
  */
  estAnnualIncome = '';
  /*
  * To store the value of
  */
  estRetirementage = '';
  /*
  * To store the value of
  */
  estAssets = '';
  /*
  * To store the value of
  */
  isSmoker = '';
  /*
  * To store the value of
  */
  isWorking = '1';
  /*
  * To store the value of
  */
  age = '';
  /*
  * To store the value of
  */
  statename = '';
  /*
  * To store the value of
  */
  gender = '';
  /*
  * variable
  */
  isSubmitCallSuccess = false;
  /*
  * variable
  */
  isMaxProtectCostLoader = false;
  /*
  * To store the value of
  */
  isInvestAst = false;
  /*
  * variable
  */
  productList: Product[] = [];
  /*
  * variable
  */
  maxCostproductList: Product[] = [];
  /*
  * variable
  */
  topThreeproductList: Product[] = [];
  /*
  * variable
  */
  incomeTankCalc: any;
  /**
   *  quotes details from group
   */
  quoteDetailsForm!: FormGroup;
  /*
  * variable
  */
  isCollapsed = false;
  /*
  * variable
  */
  isCollapsed1 = false;
  /*
  * variable
  */
  isAllReadMore = false;
  /*
  * variable
  */
  addonsReadMore = false;
  /*
  * variable
  */
  waiverReadMore = false;
  /*
  * variable
  */
  childReadMore = false;
  /*
  * variable
  */
  adbReadMore = false;
  /*
  * variable
  */
  paymentReadMore = false;
  /*
  * variable
  */
  convertReadMore = false;
  /*
 * variable
 */
  isCollapsedHealthIconUP = false;
  /*
  * variable
  */
  isCollapsedSPIconUP = false;
  /*
  * variable
  */
  isCollapsedAddOnIconUP = false;
  /*
  * variable
  */
  isCollapsedModeIconUP = false;
  /*
  * variable
  */
  isCollapsedInsurerIconUP = false;
  /*
  * variable
  */
  isCollapsedGuaranteeeIconUP = false;
  /*
  * variable
  */
  totalNumberOfProducts = 0;
  /*
  * variable
  */
  isProductsAvailable: boolean = false;
  // Pagination parameters.
  /*
  * variable
  */
  p = 1;
  /*
  * variable
  */
  count = 6;
  /*
 * variable
 */
  isDesc: boolean = false;
  /*
  * variable
  */
  isMobileDevice = false;
  /*
  * variable
  */
  isPremiumAsce: boolean = true;
  /*
  * variable
  */
  isProtectionDesc: boolean = true;
  /*
  * variable
  */
  isRatingDesc: boolean = true;
  /*
  * variable
  */
  isCNameDesc: boolean = true;
  /*
  * variable
  */
  isPremiumClicked: boolean = true;
  /*
  * variable
  */
  isProtectionClicked: boolean = false;
  /*
  * variable
  */
  isRatingClicked: boolean = false;
  /*
  * variable
  */
  isCNameClicked: boolean = false;
  /**
   * isSortingMenuOpen
   */
  isSortingMenuOpen = false;
  /*
 * variable
 */
  sortingProperties: string = 'Premium';
  /**
   * sortingBy
   */
  sortingBy = "Premium";
  /**
   *  payment mode type
   */
  paymentModeType = 'Monthly';
  /**
   * product_list 
   */
  master_product_list: any[] = [];
  /**
   *      array for health rating
   */
  isReadMore = true


  healthRating: any = [
    { id: 1, name: "Super Healthy", checked: false },
    { id: 2, name: "Healthy", checked: true },
    { id: 3, name: "Average", checked: false },
    { id: 4, name: "Fair", checked: false },
    { id: 5, name: "Poor", checked: false },
  ];
  /**
  *      array for health rating
  */
  speedProtection: any = [
    { id: 1, type: "greenplus", checked: false },
    { id: 2, type: "green", checked: false },
    { id: 3, type: "yellow", checked: false },
    { id: 4, type: "red", checked: false },
  ];
  /**
  *      array for health rating
  */
  addons: any = [
    { type: "Waiver", checked: false },
    { type: "ADB", checked: false },
    { type: "Child", checked: false }
  ];
  /**
   *      array for health rating
   */
  paymentMode: any = [
    { type: "Monthly", name: "Monthly Bank Draft", checked: true },
    { type: "Quarterly", name: "Quarterly", checked: false },
    { type: "SemiAnnual", name: "Semi-Annual", checked: false },
    { type: "Annual", name: "Annual", checked: false },
  ];
  /**
  *      array for health rating
  */
  guarantedConversion: any = [
    { isConersion: true, label: "Yes", checked: true },
    { isConersion: false, label: "No", checked: false },
  ];

  /**
   *     filter array for health rating
  */
  conersionFilter: any[] = [];
  /**
    *     filter array for health rating
   */
  healthRatingFilter: any[] = [];
  /**
   *   filter array for protection 
  */
  protectionFilter: any[] = [];
  /**
   *   filter array for protection 
  */
  addOnFilter: any[] = [];
  /**
   *     filter array for health rating
  */
  insurersFilter: any[] = [];
  /**
   *   filter array for protection 
  */
  isGridViewDisplay = true;
  /**
  *   insurers list 
 */
  insurersList: Insurers[] = [];
  /**
   *   filter array for protection 
  */
  listOfCompareProduct: any[] = [];
  /**
   * 
   */
  insurerSlice = 5;
  /**
   * 
   */
  addlInsurerlink = true;
  /**
   * var
   */
  filteredAmountOptions!: Observable<string[]>;
  filteredYearsOptions!: Observable<string[]>;
  filteredStateOptions!: Observable<any[]>;
  /**
   *  auto complete var
   */
  amountOptions: string[] = ['$25,000', '$50,000', '$75,000', '$100,000', '$125,000', '$150,000', '$200,000', '$250,000', '$300,000', '$350,000', '$400,000', '$450,000', '$500,000', '$550,000', '$600,000', '$650,000', '$700,000', '$750,000', '$800,000', '$850,000', '$900,000', '$950,000', '$1,000,000', '$2,000,000', '$3,000,000', '$4,000,000', '$5,000,000', '$6,000,000', '$7,000,000', '$8,000,000', '$9,000,000', '$10,000,000', '$11,000,000', '$12,000,000', '$13,000,000', '$14,000,000', '$15,000,000', '$16,000,000', '$17,000,000', '$18,000,000', '$19,000,000', '$20,000,000',];
  /**
    *  auto complete var
    */
  yearsOptions: string[] = ['1', '10', '15',
    '20', '25', '30', '35', '40'];
  /**
   * var to getStates
   */
  state$ = this.gondolaService.getStates().pipe(shareReplay());
  /**
   * var to getStates
   */
  statecode = '';
  /**
   * var
   */
  quoteProtectionCost = '';
  /**
   * var
   */
  maxProtectionCost = '';
  /**
   * var
   */
  dragPosition = { x: 0, y: 0 };
  /**
   * var
   */
  dragHeight = 0;
  /**
   * var
   */
  tankHeight = 0;
  /**
   * var
   */
  AdditionalProtectionAmount = 0;
  /**
   * var
   */
  updatedProtectionAmount = 0;
  /**
   * var 
   */
  potentialIncome = 0;
  /**
   * var 
   */
  maxProtectAmount = 0;
  /**
  * var 
  */
  qouteAmount = 0;
  /**
 * var 
 */
  dragableAmountDivHeight = '';
  /**
   * var 
   */
  maxProtectDivHeight = '';
  /**
  * var 
  */
  qouteAmountDivHeight = '';
  /**
   * var 
   */
  isQuoteButtonVisible = false;
  /**
   * var 
   */
  isPotentialProtectionTextVisible = true;
  /**
   * var 
   */
   isPotentialProtectionTopTextVisible = false;
  /** 
   * var
  */
  maxProtectionBoxVisible = true;

  constructor(
    private router: Router,
    private SpinnerService: NgxSpinnerService,
    private formBuilder: FormBuilder,
    private currencyPipe: CurrencyPipe,
    private dialog: MatDialog,
    public gondolaService: GondolaService,
    private sanitizer: DomSanitizer,
    private renderer: Renderer2) {
  }

  ngOnInit(): void {
    if (this.gondolaService.requestData !== null) {
      let data = this.gondolaService.requestData;
      //this.gondolaService.requestData = null;
      this.age = data.age;
      this.gender = data.gender;
      this.statename = data.statename;
      this.statecode = data.statecode;
      this.isSmoker = data.issmoker;
      this.isWorking = data.isWorking;
      this.years = data.years;
      this.amount = "$".concat(this.numberWithCommas(data.amount));
      this.estAnnualIncome = this.numberWithCommas(data.annualincome);
      this.estRetirementage = data.expectedretirementage;
      this.estAssets = this.numberWithCommas(data.estAssets);
    }
    this.quoteDetailsForm = this.formBuilder.group({
      statename: new FormControl(this.statename, { updateOn: 'change' }),
      isSmoker: new FormControl(this.isSmoker),
      isWorking: new FormControl(this.isWorking),
      gender: new FormControl(this.gender),
      amount: new FormControl(this.amount, { updateOn: 'change' }),
      age: new FormControl(this.age, { updateOn: 'change' }),
      years: new FormControl(this.years, { updateOn: 'change' }),
      estAnnualIncome: new FormControl(this.estAnnualIncome, { updateOn: 'change' }),
      estRetirementage: new FormControl(this.estRetirementage, { updateOn: 'change' }),
      estAssets: new FormControl(this.estAssets, { updateOn: 'change' }),
    });
    this.filteredAmountOptions = this.quoteDetailsForm.get('amount')?.valueChanges.pipe(
      startWith(''),
      map(value => this.amountFilter(value)),
    )!;
    this.filteredYearsOptions = this.quoteDetailsForm.get('years')?.valueChanges.pipe(
      startWith(''),
      map(value => this.yearsFilter(value)),
    )!;
    this.filteredStateOptions = this.quoteDetailsForm.get('statename')?.valueChanges.pipe(
      startWith(''),
      switchMap((name) => {
        return this.state$.pipe(
          map((stateOptions) => {
            return stateOptions.filter((st: any) =>
              st.name.toLowerCase().includes(name.toLowerCase())
            );
          })
        );
      })
    )!;
    if (this.gondolaService.productList) {
      this.master_product_list = this.gondolaService.productList;
      this.gondolaService.productList = null;
      this.mapProductDetails(this.master_product_list);
      this.initializeFilterValue();
    }

    if (window.screen.width <= 480) {
      this.isMobileDevice = true;
    }
    this.calculateIncomeTaxBoxHeight();
    // window.scroll(0, 0);    
  }
  ngAfterViewInit() {
    let secondBoxHeight = ((this.secondBoxIdentifier.nativeElement.offsetHeight) / 2);
    this.dragPosition.y = secondBoxHeight;

    if (this.maxProtectAmount > 0 && !this.gondolaService.isQuoteAndMaxAmountSame) {
      this.submitMaxProtectionQuotes(this.maxProtectAmount);
    } else {
      this.maxProtectionCost = this.quoteProtectionCost;
    }
  }

  calculateIncomeTaxBoxHeight() {
    this.potentialIncome = this.incomeTankCalc.potentialamount;
    this.maxProtectAmount = this.incomeTankCalc.maxprotectamount;
    this.qouteAmount = this.incomeTankCalc.quoteamount;
    this.updatedProtectionAmount = this.incomeTankCalc.quoteamount;
    if (this.potentialIncome > this.maxProtectAmount) {
      this.maxProtectionBoxVisible = true;
      this.maxProtectDivHeight = ((this.maxProtectAmount / this.potentialIncome) * 100).toString();
      this.qouteAmountDivHeight = ((this.qouteAmount / this.potentialIncome * 100)).toString();
      if ((+this.maxProtectDivHeight) > (+this.qouteAmountDivHeight)) {
        this.dragableAmountDivHeight = ((+this.maxProtectDivHeight) - (+this.qouteAmountDivHeight)).toString()
      } else {
        this.dragableAmountDivHeight = ((+this.qouteAmountDivHeight) - (+this.maxProtectDivHeight)).toString();
      }
      let totalTankheightInPerc =
        (+this.maxProtectDivHeight) + (+this.qouteAmountDivHeight) + (+this.dragableAmountDivHeight)
      let tankHeightAfterReduce = (totalTankheightInPerc - 100) / 2
      this.maxProtectDivHeight = ((+this.maxProtectDivHeight) - (tankHeightAfterReduce)).toString();
      this.qouteAmountDivHeight = ((+this.qouteAmountDivHeight) - (tankHeightAfterReduce)).toString();
      if(this.maxProtectDivHeight === this.qouteAmountDivHeight){
        this.isPotentialProtectionTopTextVisible = false;
      }
    } else {
      this.maxProtectionBoxVisible = false;
      this.maxProtectDivHeight = '0'
      this.qouteAmountDivHeight = ((this.qouteAmount / this.maxProtectAmount * 100)).toString();
      this.dragableAmountDivHeight = (100 - (+this.qouteAmountDivHeight)).toString()
    }
    console.log('this.qouteAmountDivHeight',this.qouteAmountDivHeight);
    console.log('this.dragableAmountDivHeight',this.dragableAmountDivHeight);
    console.log('this.maxProtectDivHeight',this.maxProtectDivHeight);
  }
  calculateDraggableBoxHeight() {
    this.dragHeight = 0;
    setTimeout(() => {
      let secondBoxHeight = ((this.secondBoxIdentifier.nativeElement.offsetHeight) / 2);
      this.dragPosition = { x: 0, y: secondBoxHeight };
      if (this.updatedProtectionAmount > this.qouteAmount) {
        this.isQuoteButtonVisible = true;
      } else {
        this.isQuoteButtonVisible = false;
      }
    });
  }
  initializeFilterValue() {
    this.guarantedConversion.forEach((con: any) => {
      if (con.isConersion) {
        this.filterConversion(con)
      }
    })
    this.healthRating.forEach((rating: any) => {
      if (rating.checked) {
        this.onChangeHealth(event, rating);
      }
    })
  }
  dragEnd($event: CdkDragEnd, maxprotection: any, qouteAmount: any) {
    //  let position: any = {};
    //  position= $event.source.getFreeDragPosition();
    // console.log(position.x,position.y)
    //  this.dragHeight = position.y;
    //console.log($event.source.getFreeDragPosition());
    let protectionTextTopPosition = document.getElementById("gl-protection-zone-text")?.getBoundingClientRect();
    let firstBoxTopPosition = document.getElementById("gondola-first-container")?.getBoundingClientRect();
    let secondBoxTopPosition = document.getElementById("gl-protection-zone")?.getBoundingClientRect();
    let draggableTopPosition = $event.source.getRootElement().getBoundingClientRect();
    this.tankHeight = this.tankIdentifier.nativeElement.offsetHeight;
    //console.log("protectionTopPosition",protectionTopPosition)
    //console.log("draggableTopPosition",draggableTopPosition)
    //this.dragHeight = this.tankHeight - (Math.round(secondBoxTopPosition.y)- Math.round(firstBoxTopPosition.y));
    this.dragHeight = (Math.round(firstBoxTopPosition!.bottom) + Math.round(secondBoxTopPosition!.height)) - Math.round(draggableTopPosition.bottom);
    //console.log("this.dragHeight",this.dragHeight,"secondBoxTopPosition.height",secondBoxTopPosition.height,"draggableTopPosition.bottom",draggableTopPosition.bottom)
    let protectionPercentage = 0;
    if (this.dragHeight === 0) {
      protectionPercentage = Math.round(this.dragHeight / secondBoxTopPosition!.height * 100);
    } else {
      // additionally 6 added to dragheight as 
      //draggable item has height of 6 which not included in dragheight
      protectionPercentage = Math.round((this.dragHeight + 17) / Math.round(secondBoxTopPosition!.height) * 100);
    }
    console.log('protectionPercentage -- ',protectionPercentage);
    let amountDiffrenece = maxprotection - qouteAmount;
    let AdditionalAmount = this.calculateProtectProportion(protectionPercentage,maxprotection,qouteAmount);
    let tempororayTotalAmount = qouteAmount + AdditionalAmount;
    this.updatedProtectionAmount = Number(tempororayTotalAmount.toFixed());
    if (this.updatedProtectionAmount > qouteAmount) {
      //this.qouteAmount = this.updatedProtectionAmount;
      this.isQuoteButtonVisible = true;
    } else {
      this.isQuoteButtonVisible = false;
    }
    console.log("protectionTextTopPosition",protectionTextTopPosition)
    console.log("draggableTopPosition.y",draggableTopPosition.y)
    if (Math.round(draggableTopPosition.y) < Math.round(protectionTextTopPosition!.y)) {
      this.isPotentialProtectionTextVisible = false;
      this.isPotentialProtectionTopTextVisible = true;
    } else {
      this.isPotentialProtectionTextVisible = true;
      this.isPotentialProtectionTopTextVisible = false;
    }
    if (protectionPercentage >=99 && !this.isPotentialProtectionTextVisible) {
      this.isPotentialProtectionTopTextVisible = false;
    }
  }
  /**
   * This amount should be added in Quote Amount
   */
  calculateProtectProportion(protectPercent:any,maxprotection: any, quoteAmount: any) : any{
    let addlAmount = 0;
    const diff = Number(maxprotection) - Number(quoteAmount);
    if(protectPercent > 0 && diff > 0){
      const amountDiffPercent= 100/(diff/25000);
      const calcRoundPercent = Math.round(protectPercent/amountDiffPercent);
      if(calcRoundPercent > 0){
        addlAmount = calcRoundPercent * 25000;
      }
    }
    return addlAmount;
  }
  /**
   * This amount should be subtract in Quote Amount
   */
  calculateQuoteProportion(protectPercent:any, quoteAmount: any) : any{
    let addlAmount = 0;
    const diff = Number(quoteAmount) - 25000;
    console.log('diff -- ',diff);
    if(diff > 0){
      const amountDiffPercent = 100/(diff/25000);
      console.log('amountDiffPercent -- ',amountDiffPercent);
      const calcRoundPercent = Math.round(protectPercent/amountDiffPercent);
      console.log('calcRoundPercent -- ',calcRoundPercent, 'protectPercent/amountDiffPercent -- ',protectPercent/amountDiffPercent);
      if(calcRoundPercent){
        addlAmount = calcRoundPercent * 25000;
      }
    }
    console.log('addlAmount -- ',addlAmount);
    return addlAmount;
  }
  headerStyle() {
    return this.sanitizer.bypassSecurityTrustStyle(
      `background: linear-gradient(to top, #618ccc ${this.dragHeight}px, rgba(0,0,0,0) 0%) !important`
    );
  }
  updateQuotes() {
    this.amount = this.numberWithCommas(this.updatedProtectionAmount);
    this.quoteDetailsForm.get('amount')?.setValue(this.amount);
    this.submitQuotes();
  }
  filterInsurerListView() {
    this.insurersList = [];
    this.insurersList.length = 0;
    this.gondolaService.getInsurersList().forEach(prd => {
      const insurer = new Insurers();
      insurer.id = prd.id;
      insurer.insurername = prd.insurername;
      insurer.insurershortname = prd.insurershortname;
      insurer.naiccode = prd.naiccode;
      insurer.isAvailable = false;
      insurer.isChecked = false;
      this.insurersList.push(insurer);
    });
    this.insurersList.forEach(item => {
      this.productList.some(prod => {
        if (prod.companyNAICCode === item.naiccode.toString()) {
          item.isAvailable = true
          return true;
        }
      });
    })
    this.insurersList.sort(function (x, y) {
      // true values first
      return (x.isAvailable === y.isAvailable) ? 0 : x.isAvailable ? -1 : 1;
      // false values first
      // return (x === y)? 0 : x? 1 : -1;
    });
  }

  private amountFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.amountOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private yearsFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.yearsOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  displayStateFn(selectedoption: any) {
    return selectedoption ? selectedoption : '';
  }

  /*
  * function to update state
  */
  updateState(event: any) {
    this.statename = event.target.value;
  }

  /*
  * function to update gender
  */
  updateGender(event: any) {
    this.gender = event.target.value;
  }

  clearReadMore() {
    this.isAllReadMore = false;
    this.addonsReadMore = false;
    this.waiverReadMore = false;
    this.adbReadMore = false;
    this.childReadMore = false;
    this.paymentReadMore = false;
    this.convertReadMore = false;
  }

  readMore(id: any, event: any) {
    event.preventDefault();
    if (id === '1') {
      this.isAllReadMore = !this.isAllReadMore;
    } else if (id === '2') {
      this.addonsReadMore = !this.addonsReadMore;
    } else if (id === '3') {
      this.waiverReadMore = !this.waiverReadMore;
    } else if (id === '4') {
      this.adbReadMore = !this.adbReadMore;
    } else if (id === '5') {
      this.childReadMore = !this.childReadMore;
    } else if (id === '6') {
      this.paymentReadMore = !this.paymentReadMore;
    } else if (id === '7') {
      this.convertReadMore = !this.convertReadMore;
    }
  }
  showInvestAst(event: any) {
    event.preventDefault();
    this.isInvestAst = !this.isInvestAst;
  }
  /*
  * function to update smoker
  */
  updateSmoker(event: any) {
    this.isSmoker = event.target.value;
  }

  updateWorking(event: any) {
    this.isWorking = event.target.value;
  }

  /*
  * function to map the product list
  */
  mapProductDetails(data: any) {
    this.isProductsAvailable = false;
    this.listOfCompareProduct = [];
    this.productList = [];
    this.topThreeproductList = [];
    this.incomeTankCalc = data.incomeTankCalc;
    this.potentialIncome = this.incomeTankCalc.potentialamount;
    this.maxProtectAmount = this.incomeTankCalc.maxprotectamount;
    this.qouteAmount = this.incomeTankCalc.quoteamount;
    this.totalNumberOfProducts = data.numberOfProductsFiltered;
    const product_list = data.filteredProductList;
    if (this.totalNumberOfProducts > 0 && product_list !== null) {
      this.isProductsAvailable = true;
      product_list.forEach((element: any) => {
        const product = new Product();
        product.productid = element.productid;
        product.productname = this.sliceChars(element.productname);
        product.productGuarenteedYears = element.productGuarenteedYears;
        product.productYearsLevel = element.productYearsLevel;
        product.productMinAgeIssued = element.productMinAgeIssued;
        product.productMaxAgeIssued = element.productMaxAgeIssued;
        product.productMinFaceAmount = element.productMinFaceAmount;
        product.productMaxFaceAmount = this.numberWithCommas(element.productMaxFaceAmount);
        product.productMaxYearsToShow = element.productMaxYearsToShow;
        product.productNAICCertified = element.productNAICCertified;
        product.companyName = element.companyName;
        product.companyNAICCertified = element.companyNAICCertified;
        product.companyNAICCode = element.companyNAICCode;
        product.companyRating = element.companyRating;
        product.guaranteedAnnualPremiumAmount = element.guaranteedAnnualPremiumAmount;
        product.guaranteedMonthlyPremiumAmount = element.guaranteedMonthlyPremiumAmount;
        product.guaranteedQuarterlyPremiumAmount = element.guaranteedQuarterlyPremiumAmount;
        product.guaranteedSemiAnnualPremiumAmount = element.guaranteedSemiAnnualPremiumAmount;
        product.healthRatingId = element.healthRatingId;
        product.healthRatingName = element.healthRatingName;
        product.speedToProtectionName = element.speedToProtectionname;
        product.companyRatingId = element.companyRatingId;
        product.waiveraddonsid = element.waiveraddonsid;
        product.waiveraddonsname = element.waiveraddonsname;
        product.adbaddonsid = element.adbaddonsid;
        product.adbaddonsname = element.adbaddonsname;
        product.childaddonsid = element.childaddonsid;
        product.childaddonsname = element.childaddonsname;
        product.speedToProtectionId = element.speedToProtectionid;
        product.isTopPick = element.topPick;
        product.allcompanyratings = element.allcompanyratings;
        product.companyAddress = element.companyAddress;
        product.companyfounded = element.companyfounded;
        product.companyphonenumber = element.companyphonenumber;
        product.companywebsite = element.companywebsite;
        product.conversionname = element.conversionname;
        product.isconversion = element.isconversion;
        product.totalpremiumvalues = element.totalpremiumvalues;
        product.faceAmount = element.faceAmount;
        this.productList.push(product);
      });
      this.topThreeproductList = data.topThreeProductList;
      this.setPageCount();
    }
  }

  mapMaxCostProductDetails(data: any) {
    this.maxProtectionCost = '';
    this.maxCostproductList = [];;
    const maxCostProduct_list = data.filteredProductList;
    if (this.totalNumberOfProducts > 0 && maxCostProduct_list !== null) {
      maxCostProduct_list.forEach((element: any) => {
        const product = new Product();
        product.guaranteedAnnualPremiumAmount = element.guaranteedAnnualPremiumAmount;
        product.guaranteedMonthlyPremiumAmount = element.guaranteedMonthlyPremiumAmount;
        product.guaranteedQuarterlyPremiumAmount = element.guaranteedQuarterlyPremiumAmount;
        product.guaranteedSemiAnnualPremiumAmount = element.guaranteedSemiAnnualPremiumAmount;
        product.healthRatingId = element.healthRatingId;
        product.healthRatingName = element.healthRatingName;
        product.isTopPick = element.topPick;
        this.maxCostproductList.push(product);
      });
    }
  }

  /*
  * function to create new request object
  */
  createNewQuoteObject(maxProtectAmount?: any): any {
    let addonlist: any = [];
    const requestObject: any = {};
    this.gondolaService.isQuoteAndMaxAmountSame = false;
    const amount = Number(this.removeAmount(this.amount));
    if (maxProtectAmount) {
      requestObject.amount = maxProtectAmount;
    } else {
      if (this.gondolaService.maxProtectionAmt > 0 && this.gondolaService.maxProtectionAmt < amount) {
        requestObject.amount = this.gondolaService.maxProtectionAmt;
        this.gondolaService.isQuoteAndMaxAmountSame = true;
      } else {
        requestObject.amount = amount;
      }
    }
    requestObject.years = this.years;
    requestObject.annualincome = this.removeCommas(this.estAnnualIncome);
    requestObject.expectedretirementage = this.estRetirementage;
    requestObject.issmoker = this.isSmoker;
    requestObject.isWorking = this.isWorking;
    requestObject.age = this.age;
    requestObject.statename = this.statename;
    requestObject.statecode = this.statecode;
    requestObject.gender = this.gender;
    requestObject.estAssets = this.removeCommas(this.estAssets);
    requestObject.ipaddress = this.gondolaService.ipAddress;
    requestObject.subscriberid = this.gondolaService.subscriberid;
    requestObject.emailaddress = this.gondolaService.emailaddress;
    requestObject.prod = this.gondolaService.isProduction;
    requestObject.maxprotectcall = maxProtectAmount ? true : false;
    if (this.addOnFilter !== null && this.addOnFilter.length > 0) {
      this.addOnFilter.forEach((e) => {
        addonlist.push(e.type);
      })
    }
    if (addonlist && addonlist.length > 0) {
      requestObject.advancefilter = true;
      requestObject.addonslist = addonlist;
    }
    return requestObject;
  }

  enteredQuotes(event: any) {
    if (event.keyCode === 13) {
      this.submitQuotes();
    }
  }

  removedPlans(event: any) {
    if (this.totalNumberOfProducts === 0) {
      // display: block;
    }
  }

  /*
  * function to submit the quotes 
  */
  submitQuotes() {
    //this.count = this.isGridViewDisplay ? 6 : 5;
    this.isSubmitDone = true;
    this.isFormValid();
    if (this.quoteDetailsForm.valid) {
      this.isSubmitCallSuccess = false;
      this.gondolaService.requestData = null;
      this.SpinnerService.show();
      this.getIncomeTankCalc();
      const QuoteObject = this.createNewQuoteObject();
      this.gondolaService.requestData = QuoteObject;
      this.gondolaService.createQuotes(QuoteObject).subscribe(
        resObj => {
          if (resObj !== null) {
            this.setQuoteInfo(resObj);
            this.calculateIncomeTaxBoxHeight()
            this.calculateDraggableBoxHeight();
          }
        },
        error => {
          this.SpinnerService.hide();
        });
    }
    window.scroll(0, 0);
  }

  getIncomeTankCalc() {
    this.gondolaService.getMaxProtectionCalc(Number(this.age), this.isWorking, this.removeCommas(this.estAssets), this.removeCommas(this.estAnnualIncome));
    this.gondolaService.getPotentialCalc(Number(this.age), this.isWorking, this.removeCommas(this.estAssets), this.removeCommas(this.estAnnualIncome), Number(this.estRetirementage));
  }

  submitMaxProtectionQuotes(maxProtectAmount: any) {
    this.isSubmitCallSuccess = false;
    this.isMaxProtectCostLoader = true;
    const QuoteObject = this.createNewQuoteObject(maxProtectAmount);
    this.gondolaService.createQuotes(QuoteObject).subscribe(
      resObj => {
        if (resObj !== null) {
          this.setMaxCostQuoteInfo(resObj);
        }
      },
      error => {
        this.isSubmitCallSuccess = true;
        this.isMaxProtectCostLoader = false;
      });
  }


  isFormValid() {
    this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
    this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
    this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    this.quoteDetailsForm.get('age')?.setValidators(this.validateAge());
    this.quoteDetailsForm.get('age')?.updateValueAndValidity();
    this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
    this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    this.quoteDetailsForm.get('estAnnualIncome')?.setValidators(this.validateIncome());
    this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
    if (this.isWorking === '1') {
      this.quoteDetailsForm.get('estRetirementage')?.setValidators(this.validateRetireAge());
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
      this.quoteDetailsForm.get('estAssets')?.clearValidators();
      this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
    } else if (this.isWorking === '2') {
      this.quoteDetailsForm.get('estAssets')?.setValidators(this.validateEstAsset());
      this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
      this.quoteDetailsForm.get('estRetirementage')?.clearValidators();
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
    }
  }

  validateOnSelect(type: string, event?: any) {
    if (type === 'amount') {
      this.amount = event;
      this.amount = this.removeDollar(this.amount);
      this.quoteDetailsForm.get('amount')?.setValue(this.amount);
      this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
      this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
    }
    else if (type === 'years') {
      this.years = event;
      this.quoteDetailsForm.get('years')?.setValue(this.years);
      this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
      this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    } else if (type === 'state') {
      this.statename = event;
      this.quoteDetailsForm.get('statename')?.setValue(this.statename);
      this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
      this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    }
  }

  validateOnKeyUp(type: string, event?: any) {
    if (type === 'years') {
      this.years = event.target.value;
      this.quoteDetailsForm.get('years')?.setValue(this.years);
      this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
      this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    } else if (type === 'age') {
      this.age = event.target.value;
      this.quoteDetailsForm.get('age')?.setValue(this.age);
      this.quoteDetailsForm.get('age')?.setValidators(this.validateAge());
      this.quoteDetailsForm.get('age')?.updateValueAndValidity();
    } else if (type === 'state') {
      this.statename = event.target.value;
      this.quoteDetailsForm.get('statename')?.setValue(this.statename);
      this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
      this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    } else if (type === 'estretireage') {
      this.estRetirementage = event.target.value;
      this.quoteDetailsForm.get('estRetirementage')?.setValue(this.estRetirementage);
      this.quoteDetailsForm.get('estRetirementage')?.setValidators(this.validateRetireAge());
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
    }
  }

  popupCheck(): boolean {
    let isValid: boolean = true;
    let errorInfo: any = '';
    if (this.amount) {
      const amountVal = Number(this.removeCommas(this.amount));
      if (amountVal < 100000) {
        isValid = false;
        errorInfo = 'amount';
      }
    }
    if (this.years && Number(this.years) < 10) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'years';
    }
    if (this.age && (Number(this.age) < 18 || Number(this.age) > 80)) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'age';
    } if (this.statename) {
      if (this.gondolaService.States !== null) {
        this.gondolaService.States.forEach((e) => {
          if (e.name.toLowerCase() === this.statename.toLowerCase()) {
            this.statecode = e.code;
          }
        });
        if (!this.statecode) {
          isValid = false;
          errorInfo += errorInfo ? '$' : '';
          errorInfo += 'state';
        }
      }
    }
    if (this.estAnnualIncome) {
      const incomeVal = Number(this.removeCommas(this.estAnnualIncome));
      if (incomeVal < 25000) {
        isValid = false;
        errorInfo += errorInfo ? '$' : '';
        errorInfo += 'income';
      }
    }
    if (this.isWorking === '1' && this.estRetirementage && Number(this.estRetirementage) < Number(this.age)) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'estretireage';
    }
    if (this.isWorking === '2' && this.estAssets) {
      const incomeVal = Number(this.removeCommas(this.estAssets));
      if (incomeVal < 100000) {
        isValid = false;
        errorInfo += errorInfo ? '$' : '';
        errorInfo += 'estassets';
      }
    }
    if (!isValid) {
      this.openErrorPopup(errorInfo);
    }
    return isValid;
  }

  openErrorPopup(data: any) {
    const dialogRef = this.dialog.open(ErrorPopupComponent, {
      data: {
        message: data,
      },
    });
  }

  setPageCount() {
    if (this.isGridViewDisplay) {
      if (this.count == 100) {
        this.count = 100;
      } else if (this.count == 6 || this.totalNumberOfProducts <= 6) {
        this.count = 6;
      } else if (this.count == 12 || this.totalNumberOfProducts <= 12) {
        this.count = 12;
      } else if (this.count == 18 || this.totalNumberOfProducts <= 18) {
        this.count = 18;
      } else if (this.count == 24 || this.totalNumberOfProducts <= 24) {
        this.count = 24;
      }
    } else {
      if (this.count == 100) {
        this.count = 100;
      } else if (this.count == 5 || this.totalNumberOfProducts <= 5) {
        this.count = 5;
      } else if (this.count == 10 || this.totalNumberOfProducts <= 10) {
        this.count = 10;
      } else if (this.count == 15 || this.totalNumberOfProducts <= 15) {
        this.count = 15;
      } else if (this.count == 25 || this.totalNumberOfProducts <= 25) {
        this.count = 25;
      }
    }
  }

  sliceChars(name: string): string {
    let s: any = [];
    if (name) {
      s = name.split("(");
    }
    return s[0];
  }

  setQuoteInfo(data: any) {
    if (data !== null) {
      this.maxProtectAmount = 0;
      this.master_product_list = [];
      this.healthRatingFilter = [];
      this.SpinnerService.hide();
      this.master_product_list = data;
      this.mapProductDetails(data);
      this.initializeFilterValue();
      this.getTopPickProduct();
      this.clearHealthFilter();
      if (this.maxProtectAmount > 0 && !this.gondolaService.isQuoteAndMaxAmountSame) {
        this.submitMaxProtectionQuotes(this.maxProtectAmount);
      } else {
        this.maxProtectionCost = this.quoteProtectionCost;
      }
    }
  }


  setMaxCostQuoteInfo(data: any) {
    if (data !== null) {
      this.mapMaxCostProductDetails(data);
      if (this.maxCostproductList) {
        this.getMaxCostTopPickProduct();
      }
    }
    this.isMaxProtectCostLoader = false;
  }

  removeDecimalValues(amount: any) {
    return this.numberWithCommas(Number(amount.replace("$", "").replace(",", "")));
  }

  removeDollar(amount: string) {
    return amount ? amount.toString().replace(/\$/g, '') : '';
  }

  removeAmount(amount: string): string {
    let amt = '';
    if (amount && null !== amount) {
      amt = amount.replace(/\$/g, '');
      amt = this.removeCommas(amt);
    }
    return amt;
  }

  updatePageView(event: any) {
    this.count = event.target.value;
    this.p = 1;
    //window.scroll(0,0);
  }

  updatePage(event: any) {
    this.p = event;
    //window.scroll(0,0);
  }

  checkInputValue(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  /*@HostListener('document:click',['$event'])
  clickOutSide(event:any) {
        if(event.target !== this.sortingToggleButton.nativeElement){
         this.isSortingMenuOpen=false;
     }
  }*/
  changeViewForQuotesList(event: any, viewType: string) {
    event.preventDefault();
    if (viewType === 'Grid') {
      this.isGridViewDisplay = true;
    } else if (viewType === 'List') {
      this.isGridViewDisplay = false;
    }
    this.setPageCount();
  }
  sortingToggleMenu(event: any) {
    event.preventDefault();
    this.isSortingMenuOpen = !this.isSortingMenuOpen;
  }


  navigateToSection(el: HTMLElement) {
    el.scrollIntoView({ behavior: 'smooth' });
  }

  defaultSortedProdList() {
    let direction = 1;
    this.productList.sort((a, b) => {
      if (Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) <
        Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
        return -1 * direction;
      }
      else if (Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) >
        Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
        return 1 * direction;
      }
      else {
        return 0;
      }
    }).sort((a, b) => {
      if (a.speedToProtectionId < b.speedToProtectionId) {
        return -1 * direction;
      }
      else if (a.speedToProtectionId > b.speedToProtectionId) {
        return 1 * direction;
      }
      else {
        return 0;
      }
    }).sort((a, b) => {
      if (a.companyRatingId < b.companyRatingId) {
        return -1 * direction;
      }
      else if (a.companyRatingId > b.companyRatingId) {
        return 1 * direction;
      }
      else {
        return 0;
      }
    });
  }
  sortByOrder(event: any, property: any) {
    event.preventDefault();
    this.sortingProperties = property;
    if (this.sortingProperties === 'Premium') {
      this.isPremiumClicked = true;
      this.isProtectionClicked = this.isRatingClicked = this.isCNameClicked = false;
      this.isPremiumAsce = !this.isPremiumAsce; //change the direction
      let direction = this.isPremiumAsce ? 1 : -1;
      this.paymentMode.forEach((mode: any) => {
        if (mode.checked && mode.type === 'Monthly') {
          this.productList.sort((a, b) => {
            if (Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) <
              Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return -1 * direction;
            }
            else if (Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) >
              Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return 1 * direction;
            }
            else {
              return 0;
            }
          })
        } else if (mode.checked && mode.type === 'Quarterly') {
          this.productList.sort((a, b) => {
            if (Number(a.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, '')) <
              Number(b.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return -1 * direction;
            }
            else if (Number(a.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, '')) >
              Number(b.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return 1 * direction;
            }
            else {
              return 0;
            }
          })
        } else if (mode.checked && mode.type === 'SemiAnnual') {
          this.productList.sort((a, b) => {
            if (Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) <
              Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return -1 * direction;
            }
            else if (Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) >
              Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return 1 * direction;
            }
            else {
              return 0;
            }
          })
        } else if (mode.checked && mode.type === 'Annual') {
          this.productList.sort((a, b) => {
            if (Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) <
              Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return -1 * direction;
            }
            else if (Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) >
              Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''))) {
              return 1 * direction;
            }
            else {
              return 0;
            }
          })
        }
      });
    } else if (this.sortingProperties === 'speedToProtection') {
      this.isProtectionClicked = true;
      this.isPremiumClicked = this.isRatingClicked = this.isCNameClicked = false;
      this.isProtectionDesc = !this.isProtectionDesc; //change the direction
      let direction = this.isProtectionDesc ? 1 : -1;
      this.productList.sort((a, b) => {
        if (a.speedToProtectionId < b.speedToProtectionId) {
          return -1 * direction;
        }
        else if (a.speedToProtectionId > b.speedToProtectionId) {
          return 1 * direction;
        }
        else {
          return 0;
        }
      })
    } else if (this.sortingProperties === 'CompanyRating') {
      this.isRatingClicked = true;
      this.isPremiumClicked = this.isProtectionClicked = this.isCNameClicked = false;
      this.isRatingDesc = !this.isRatingDesc; //change the direction
      let direction = this.isRatingDesc ? 1 : -1;
      this.productList.sort((a, b) => {
        if (a.companyRatingId < b.companyRatingId) {
          return -1 * direction;
        }
        else if (a.companyRatingId > b.companyRatingId) {
          return 1 * direction;
        }
        else {
          return 0;
        }
      });
    } else if (this.sortingProperties === 'CompanyName') {
      this.isCNameClicked = true;
      this.isPremiumClicked = this.isProtectionClicked = this.isRatingClicked = false;
      this.isCNameDesc = !this.isCNameDesc; //change the direction
      let direction = this.isCNameDesc ? 1 : -1;
      this.productList.sort((a, b) => {
        if (a.companyName < b.companyName) {
          return -1 * direction;
        }
        else if (a.companyName > b.companyName) {
          return 1 * direction;
        }
        else {
          return 0;
        }
      });
    }
  }

  getMaxCostTopPickProduct() {
    this.maxCostproductList = this.maxCostproductList.filter(prod => {
      return this.healthRatingFilter.findIndex((rat: any) => {
        return prod.healthRatingId === rat.id;
      }) !== -1;
    });
    this.paymentMode.forEach((mode: any) => {
      if (mode.checked && mode.type === 'Monthly') {
        this.maxCostproductList.sort((a, b) => {
          return Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) -
            Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''));
        }).forEach((prod, index) => {
          if (index == 0) {
            prod.isTopPick = true;
          } else {
            prod.isTopPick = false;
          }
        })
      }
    })
    this.setMaxProtectionCost();
  }
  getTopPickProduct() {
    this.isPremiumAsce = !this.isPremiumAsce; //change the direction
    this.paymentMode.forEach((mode: any) => {
      if (mode.checked && mode.type === 'Monthly') {
        this.productList.sort((a, b) => {
          return Number(a.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, '')) -
            Number(b.guaranteedMonthlyPremiumAmount.replace(/(^\$|,)/g, ''));
        }).forEach((prod, index) => {
          if (index == 0) {
            prod.isTopPick = true;
          } else {
            prod.isTopPick = false;
          }
        })
      } else if (mode.checked && mode.type === 'Quarterly') {
        this.productList.sort((a, b) => {
          return Number(a.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, '')) -
            Number(b.guaranteedQuarterlyPremiumAmount.replace(/(^\$|,)/g, ''));
        }).forEach((prod, index) => {
          if (index == 0) {
            prod.isTopPick = true;
          } else {
            prod.isTopPick = false;
          }
        })
      } else if (mode.checked && mode.type === 'SemiAnnual') {
        this.productList.sort((a, b) => {
          return Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) -
            Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''));
        }).forEach((prod, index) => {
          if (index == 0) {
            prod.isTopPick = true;
          } else {
            prod.isTopPick = false;
          }
        })
      } else if (mode.checked && mode.type === 'Annual') {
        this.productList.sort((a, b) => {
          return Number(a.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, '')) -
            Number(b.guaranteedSemiAnnualPremiumAmount.replace(/(^\$|,)/g, ''));
        }).forEach((prod, index) => {
          if (index == 0) {
            prod.isTopPick = true;
          } else {
            prod.isTopPick = false;
          }
        })
      }
    });
  }
  getfilterInsureres(event: any, insurersType: any) {
    const checked = event.target.checked;
    if (checked) {
      this.insurersFilter = [...this.insurersFilter, { ...insurersType }];
      this.insurersList.forEach((ele: any) => {
        if (ele.naiccode === insurersType.naiccode) {
          ele.isChecked = true;
        }
      });
    } else {
      this.insurersFilter = [...this.insurersFilter.filter(x => x.naiccode !== insurersType.naiccode)];
    }
    this.filterProductByCatogory();
  }
  onChangeMode(event: any, modeType: any) {
    const checked = event.target.checked;
    if (checked) {
      this.paymentModeType = modeType.type;
      this.paymentMode.forEach((mode: any) => {
        if (mode.type === modeType.type) {
          mode.checked = true;
        } else {
          mode.checked = false;
        }
      });
    } else {
      event.target.checked = true;
    }
  }
  onChangeHealth(event?: any, healthType?: any) {
    if (event.target.type === "checkbox") {
      const checked = event.target.checked;
      if (checked) {
        this.healthRatingFilter = [];
        this.healthRatingFilter = [...this.healthRatingFilter, { ...healthType }];
        this.healthRating.forEach((ele: any) => {
          if (ele.id === healthType.id) {
            ele.checked = true;
          } else {
            ele.checked = false;
          }
        });
      } else {
        //this.healthRatingFilter = [...this.healthRatingFilter.filter(x => x.id !== healthType.id)];
        event.target.checked = true;
      }
    } else {
      this.healthRatingFilter = [...this.healthRatingFilter, { ...healthType }];
    }
    this.filterProductByCatogory();
    this.setQuoteProductionCost();
  }

  setQuoteProductionCost() {
    const quoteCost = this.productList.filter(x => x.isTopPick && x.healthRatingId === 2);
    quoteCost.forEach(e => {
      this.quoteProtectionCost = e.guaranteedMonthlyPremiumAmount;
    })
  }
  setMaxProtectionCost() {
    let maxProtectCost = this.maxCostproductList.filter(x => x.isTopPick && x.healthRatingId === 2);
    if (!maxProtectCost) {
      maxProtectCost = this.maxCostproductList.filter(x => x.isTopPick);
    }
    maxProtectCost.forEach(e => {
      this.maxProtectionCost = e.guaranteedMonthlyPremiumAmount;
    })
  }
  onChangeProtection(event: any, protectionType: any) {
    const checked = event.target.checked;
    if (checked) {
      this.protectionFilter = [...this.protectionFilter, { ...protectionType }];
      this.speedProtection.forEach((ele: any) => {
        if (ele.type === protectionType.type) {
          ele.checked = true;
        }
      });
    } else {
      this.protectionFilter = [...this.protectionFilter.filter(x => x.type !== protectionType.type)];
    }
    this.filterProductByCatogory();
  }
  onChangeAddOns(event: any, AddOnType: any) {
    const checked = event.target.checked;
    if (checked) {
      this.addOnFilter = [...this.addOnFilter, { ...AddOnType }];
    } else {
      this.addOnFilter = [...this.addOnFilter.filter(x => x.id !== AddOnType.id)];
    }
    this.filterProductByCatogory(AddOnType.id);
  }

  getAddonsFilter(event: any, AddOnType: any) {
    const checked = event.target.checked;
    if (checked) {
      this.addOnFilter.push(AddOnType);
      this.addons.forEach((ele: any) => {
        if (ele.type === AddOnType.type) {
          ele.checked = true;
        }
      });
    } else {
      this.addOnFilter = [...this.addOnFilter.filter(x => x !== AddOnType)];
    }
    this.submitQuotes();
  }

  filterConversion(conversionType: any) {
    this.conersionFilter = [conversionType];
    this.filterProductByCatogory();
  }


  clearHealthFilter() {
    this.healthRatingFilter = [];
    this.healthRating.forEach((ele: any, index: any) => {
      if (index == 1) {
        ele.checked = true;
        this.healthRatingFilter = [...this.healthRatingFilter, { ...ele }];
      } else {
        ele.checked = false;
      }
    })
    this.filterProductByCatogory();
  }
  clearProtectionFilter() {
    this.protectionFilter = [];
    this.speedProtection.forEach((ele: any) => {
      ele.checked = false;
    })
    this.filterProductByCatogory();
  }
  clearAddOnFilter() {
    this.addOnFilter = [];
    this.addons.forEach((ele: any, index: any) => {
      if (index !== 0) {
        ele.checked = false;
      }
    })
    this.filterProductByCatogory();
  }
  clearPaymentFilter() {
    this.paymentModeType = 'Monthly';
    this.paymentMode.forEach((ele: any, index: any) => {
      if (index == 0) {
        ele.checked = true;
      } else {
        ele.checked = false;
      }
    })
  }

  clearInsurersFilter() {
    this.insurersFilter = [];
    this.insurersList.forEach((ele: any) => {
      ele.isChecked = false;
    })
    this.filterProductByCatogory();
  }

  clearAllFilter() {
    this.clearHealthFilter();
    this.clearPaymentFilter();
    this.clearProtectionFilter();
    this.clearAddOnFilter();
    this.clearInsurersFilter();
  }

  filterProductByCatogory(addOnid?: any): any[] {
    this.mapProductDetails(this.master_product_list);
    if (this.healthRatingFilter.length > 0) {
      this.productList = this.productList.filter(prod => {
        return this.healthRatingFilter.findIndex((rat: any) => {
          return prod.healthRatingId === rat.id;
        }) !== -1;
      });
    } else {
      this.productList = this.productList.filter(prod => {
        return this.healthRatingFilter.findIndex((rat: any) => {
          return prod.healthRatingId === rat.id;
        }) !== 1;
      });
    }
    if (this.protectionFilter.length > 0) {
      this.productList = this.productList.filter(prod => {
        return this.protectionFilter.findIndex((protect: any) => {
          return prod.speedToProtectionName === protect.type;
        }) !== -1;
      });
    } else {
      this.productList = this.productList.filter(prod => {
        return this.protectionFilter.findIndex((protect: any) => {
          return prod.speedToProtectionName === protect.type;
        }) !== 1;
      });
    }
    if (this.insurersFilter.length > 0) {
      this.productList = this.productList.filter(prod => {
        return this.insurersFilter.findIndex((insurer: any) => {
          return prod.companyNAICCode === insurer.naiccode.toString();
        }) !== -1;
      });
    } else {
      this.productList = this.productList.filter(prod => {
        return this.insurersFilter.findIndex((insurer: any) => {
          return prod.companyNAICCode === insurer.naiccode.toString();
        }) !== 1;
      });
    }
    if (this.addOnFilter.length > 0) {
      if (addOnid === 1) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.waiveraddonsid === addon.id;
          }) !== -1;
        });
      }
      if (addOnid === 2) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.adbaddonsid === addon.id;
          }) !== -1;
        });
      }
      if (addOnid === 3) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.childaddonsid === addon.id;
          }) !== -1;
        });
      }
    } else {
      if (addOnid === 1) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.waiveraddonsid === addon.id;
          }) !== 1;
        });
      }
      if (addOnid === 2) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.adbaddonsid === addon.id;
          }) !== 1;
        });
      }
      if (addOnid === 3) {
        this.productList = this.productList.filter(prod => {
          return this.addOnFilter.findIndex((addon: any) => {
            return prod.childaddonsid === addon.id;
          }) !== 1;
        });
      }
    }
    if (this.conersionFilter.length > 0) {
      this.productList = this.productList.filter(prod => {
        return this.conersionFilter.findIndex((con: any) => {
          return prod.isconversion === con.isConersion;
        }) !== -1;
      });
    }
    this.totalNumberOfProducts = this.productList.length;
    if (this.insurersFilter.length == 0) {
      this.filterInsurerListView();
    }
    this.getTopPickProduct();
    return this.productList;
  }



  validateAmount(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const amountVal = Number(this.removeAmount(this.amount));
      if (!control.value) {
        return { required: true }
      } else if (amountVal < 25000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('amount')?.clearValidators();
        this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateYears(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (Number(this.years) < 10 && Number(this.years) != 1) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('years')?.clearValidators();
        this.quoteDetailsForm.get('years')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateAge(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (!(Number(this.age) >= 18 && Number(this.age) <= 80)) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('age')?.clearValidators();
        this.quoteDetailsForm.get('age')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateState(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      this.setStateCode();
      if (!control.value) {
        return { required: true }
      } else if (!this.statecode) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('statename')?.clearValidators();
        this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateIncome(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const incomeVal = Number(this.removeCommas(this.estAnnualIncome));
      if (!control.value) {
        return { required: true }
      } else if (incomeVal < 25000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estAnnualIncome')?.clearValidators();
        this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateRetireAge(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (Number(this.estRetirementage) < Number(this.age) || Number(this.estRetirementage) > 120) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estRetirementage')?.clearValidators();
        this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateEstAsset(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const assetVal = Number(this.removeCommas(this.estAssets));
      if (!control.value) {
        return { required: true }
      } else if (assetVal < 100000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estAssets')?.clearValidators();
        this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
        return null;
      }
    };
  }

  setStateCode() {
    this.statecode = '';
    if (this.statename && this.gondolaService.States !== null) {
      this.gondolaService.States.forEach((e) => {
        if (e.name.toLowerCase() === this.statename.toLowerCase()) {
          this.statecode = e.code;
          return true;
        }
      });
    }

  }

  addToCompare(event: any, product: any) {
    const checked = event.target.checked;
    if (checked) {
      this.listOfCompareProduct.push(product);
    } else {
      this.listOfCompareProduct.forEach((prod, index) => {
        if (prod.productid === product.productid) {
          this.listOfCompareProduct.splice(index, 1);
        }
      })
    }
  }


  convertAmount(event: any) {
    this.amount = event.target.value;
    this.amount = this.removeDollar(this.amount);
    this.quoteDetailsForm.get('amount')?.setValue(this.amount);
    if (this.amount !== null && this.amount !== '' && this.amount !== undefined && this.isNumeric(this.amount)) {
      this.amount = this.numberWithCommas(this.amount.replace(/,/g, ''));
      this.amount = '$'.concat(this.amount);
      this.quoteDetailsForm.get('amount')?.setValue(this.amount);
    }
    this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
  }


  convertIncome(event: any) {
    this.estAnnualIncome = event.target.value;
    this.quoteDetailsForm.get('estAnnualIncome')?.setValue(this.estAnnualIncome);
    if (this.estAnnualIncome !== null && this.estAnnualIncome !== '' && this.estAnnualIncome !== undefined && this.isNumeric(this.estAnnualIncome)) {
      this.estAnnualIncome = this.numberWithCommas(this.estAnnualIncome.replace(/,/g, ''));
      this.quoteDetailsForm.get('estAnnualIncome')?.setValue(this.estAnnualIncome);
    }
    this.quoteDetailsForm.get('estAnnualIncome')?.setValidators(this.validateIncome());
    this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
  }

  convertAssets(event: any) {
    this.estAssets = event.target.value;
    this.quoteDetailsForm.get('estAssets')?.setValue(this.estAssets);
    if (this.estAssets !== null && this.estAssets !== '' && this.estAssets !== undefined && this.isNumeric(this.estAssets)) {
      this.estAssets = this.numberWithCommas(this.estAssets.replace(/,/g, ''));
      this.quoteDetailsForm.get('estAssets')?.setValue(this.estAssets);
    }
    this.quoteDetailsForm.get('estAssets')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
  }

  numberWithCommas(e: any) {
    let number = e;
    if (number && null !== number) {
      number = number?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    return number;
  }

  isNumeric(e: any): boolean {
    if (/^(\d+)?([.]?\d{0,9})?$/.test(this.removeCommas(e))) return true;
    else return false;
  }

  removeCommas(e: any) {
    return e.toString().replace(/,/g, '');
  }

  onFocusAmount(e: any) {
    this.amount = e.target.value;
  }

  onFocusIncome(e: any) {
    this.estAnnualIncome = e.target.value;
  }

  allowNumeric(event: any) {
    const pattern = /^[0-9]{1,9}$/;
    if ((event.type === 'keypress' && !pattern.test(event.key)) ||
      (event.type === 'paste' && !pattern.test(event.clipboardData.getData('Text')))) {
      event.preventDefault();
    }
  }

  openLoginBox() {
    const dialogRef = this.dialog.open(LoginModelComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        message: 'login',
      },
    });
  }

  addOnsModal(): void {
    /*const dialogRef = this.dialog.open(AddOnsTestComponent
    );
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });*/
  }

  removedPlansModal(): void {
    const dialogRef = this.dialog.open(RemovedPlansComponent, {
      panelClass: 'removed-plans-dialog-container',
      // data: {
      //   isMobileDevice: this.isMobileDevice,
      // },
    });
  }
  betalaunchModal(): void {
    const dialogRef = this.dialog.open(BetaLaunchComponent, {
      panelClass: 'betalaunch-dialog-container',
      data: {
        isMobileDevice: this.isMobileDevice,
      },
    });
  }

  openProductDetails(productInfo: any) {
    const dialogRef = this.dialog.open(ProductDetailsComponent, {
      panelClass: 'productdetails-dialog-container',
      data: {
        productInfo: productInfo,
        paymentMode: this.paymentModeType,
        reqObj: this.gondolaService.requestData,
      },
    });
  }

  openAddOns() {
    const dialogRef = this.dialog.open(AddonsPopupComponent, {
      data: {
        message: 'addons',
      },
    });
  }

  navigateToApply() {
    window.scroll(0, 0);
    this.pageName = 'register';
  }
  moveToNext() {
    window.scroll(0, 0);
    this.pageView = this.pageView + 1;
  }
  moveToBack() {
    window.scroll(0, 0);
    if (this.pageView === 1) {
      this.pageName = 'quotes';
    } else {
      this.pageView = this.pageView - 1;
    }
  }
}

class Product {
  /*
  * variable
  */
  companyNAICCertified = '';
  /*
  * variable
  */
  companyNAICCode = '';
  /*
  * variable
  */
  companyName = '';
  /*
  * variable
  */
  companyRating = '';
  /*
  * variable
  */
  guaranteedAnnualPremiumAmount = '';
  /*
  * variable
  */
  guaranteedMonthlyPremiumAmount = '';
  /*
  * variable
  */
  guaranteedQuarterlyPremiumAmount = '';
  /*
  * variable
  */
  guaranteedSemiAnnualPremiumAmount = '';
  /*
  * variable
  */
  productGuarenteedYears = '';
  /*
  * variable
  */
  productYearsLevel = '';
  /*
  * variable
  */
  productMaxAgeIssued = '';
  /*
  * variable
  */
  productMaxFaceAmount = '';
  /*
  * variable
  */
  productMaxYearsToShow = '';
  /*
  * variable
  */
  productMinAgeIssued = '';
  /*
  * variable
  */
  productMinFaceAmount = '';
  /*
  * variable
  */
  productNAICCertified = false;
  /*
  * variable
  */
  productid = '';
  /*
  * variable
  */
  productname = '';
  /*
  * variable
  */
  healthRatingId: number = 0;
  /*
  * variable
  */
  healthRatingName = ''
  /*
  * variable
  */
  companyRatingId: number = 0;
  /*
   * variable
   */
  speedToProtectionId: number = 0;
  /*
  * variable
  */
  speedToProtectionName = '';
  /*
  * variable
  */
  waiveraddonsid: number = 0;
  /*
  * variable
  */
  waiveraddonsname = '';
  /*
  * variable
  */
  adbaddonsid: number = 0;
  /*
  * variable
  */
  adbaddonsname = '';
  /*
  * variable
  */
  childaddonsid: number = 0;
  /*
  * variable
  */
  childaddonsname = '';
  /*
  * variable
  */
  isTopPick = false;
  /*
  * variable
  */
  allcompanyratings: any;
  /*
  * variable
  */
  companyfounded = '';
  /*
  * variable
  */
  companyphonenumber = '';
  /*
  * variable
  */
  companywebsite = '';
  /*
  * variable
  */
  conversionname = '';
  /*
  * variable
  */
  isconversion = false;
  /*
  * variable
  */
  companyAddress: any;
  /*
  * variable
  */
  totalpremiumvalues: any;
  /*
  * variable
  */
  faceAmount = '';
}

class Insurers {
  id: number = 0;
  insurername = '';
  insurershortname = '';
  naiccode = '';
  isAvailable = false;
  isChecked = false;
}


