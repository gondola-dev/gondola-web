import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { LoginModelComponent } from 'src/app/home-section/login-model/login-model.component';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-apply-quotes',
  templateUrl: './apply-quotes.component.html',
  styleUrls: ['./apply-quotes.component.scss']
})
export class ApplyQuotesComponent implements OnInit {

  constructor(private router : Router,
    public gondolaService: GondolaService,
    private dialog: MatDialog,) { }

  pageView = 1;
  pageName= '';

  ngOnInit(): void {
    window.scroll(0,0);
  }
  

  moveToNext(){
    window.scroll(0,0);
    this.pageView = this.pageView + 1;
  }
  moveToBack(){
    window.scroll(0,0);
    if(this.pageView ===1){
      this.pageName = 'quotes';
    } else {
      this.pageView = this.pageView - 1;
    }
  }
}
