import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyQuotesComponent } from './apply-quotes.component';

describe('ApplyQuotesComponent', () => {
  let component: ApplyQuotesComponent;
  let fixture: ComponentFixture<ApplyQuotesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApplyQuotesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
