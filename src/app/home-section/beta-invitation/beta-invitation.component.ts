import { Component, OnInit, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { LoginModelComponent } from '../login-model/login-model.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { GondolaService } from 'src/app/services/gondola.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxOtpInputConfig } from 'ngx-otp-input';
import { GondieTesterPopupComponent } from '../../popups/gondie-tester-popup/gondie-tester-popup.component';

@Component({
  selector: 'app-beta-invitation',
  templateUrl: './beta-invitation.component.html',
  styleUrls: ['./beta-invitation.component.scss']
})
export class BetaInvitationComponent implements OnInit {

  isCollapsed = false;

  name = '';

  email = '';

  mobileno = '';

  placeHolder="Enter Email ID";

  isMainPage = true;

  isOTPPage = false;

  isRegisterPage = false;

  isSubmitDone = false;

  isApiError = false;
  otpValue = '';
  isOtpNotFilled= false;
  isOTPVerifyFailed = false;
  isResendOTP = false;
  isResendOTPFailed =false;
  /**
   *  quotes details from group
   */
  subscriberForm!: FormGroup;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    public gondolaService: GondolaService,
    private SpinnerService: NgxSpinnerService,
    private dialog: MatDialog) { 
    }

  ngOnInit(): void {
    this.gondolaService.IsThisUs = false;
    this.gondolaService.IsInvite = true;
    this.gondolaService.isHome = false;
    this.gondolaService.IsCalc = false;
    this.subscriberForm = this.formBuilder.group({
      name: new FormControl(this.name, Validators.required),
      email: new FormControl(this.email, [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      mobileno: new FormControl(this.mobileno),
    });
    this.subscriberForm.get('email')?.valueChanges.subscribe((event) => {
      this.subscriberForm.get('email')?.setValue(event.toLowerCase(), {emitEvent: false});
   })
  }
  otpInputConfig: NgxOtpInputConfig = {
    otpLength: 6,
    autofocus: true,
    classList: {
      inputBox: 'my-super-box-class',
      input: 'my-super-class',
      inputFilled: 'my-super-filled-class',
      inputDisabled: 'my-super-disable-class',
      inputSuccess: 'my-super-success-class',
      inputError: 'my-super-error-class',
    },
  };

  handeOtpChange(value: string[]): void {
    this.otpValue = '';
    value.forEach(val => {
      if(val){
        this.otpValue += val;
      }
    })
  }

 /* handleFillEvent(value: string): void {
    this.otpValue = value;
  }*/

  validateOTP() {
    this.isOtpNotFilled = false;
    this.isOTPVerifyFailed = false;
    if(this.otpValue && this.otpValue.length === 6){
      const reqObj = this.createRequestObject(true);
      this.SpinnerService.show()
      this.gondolaService.validateOTP(reqObj).subscribe(
        resObj => {
          if (resObj !== null) {
            const resObject: any = resObj;
            if(resObject.status === 'success'){
              this.openGondieTesterPopup();
            } else if(resObject.status === 'failure'){
              this.isOTPVerifyFailed = true;
            }
            this.SpinnerService.hide();
          }
        },
        error => {
          this.SpinnerService.hide();
          this.isApiError = true;
        });
    } else {
      this.isOtpNotFilled = true;
    }
  }
  
  navigateToHome(){
    window.scroll(0,0);
    this.router.navigate(['']);
  }

  submitSubscriberInfo(){
    this.isApiError = false;
    this.isOTPPage = false;
    this.isSubmitDone = true;
    if(this.subscriberForm.valid){
      const reqObj = this.createRequestObject();
      this.SpinnerService.show()
      this.gondolaService.createSubscriberInfo(reqObj).subscribe(
        resObj => {
          if (resObj !== null) {
            console.log('resObj --',resObj);
            const resObject: any = resObj;
            if(resObject.status === 'success'){
              if(resObject.pageName === 'otpPage'){
                this.isMainPage = false;
                this.isOTPPage = true;
              } else if(resObject.pageName === 'homePage'){
                this.navigateToHome();
              }
            } else if(resObject.status === 'failure'){
              this.isApiError = true;
            }
            this.SpinnerService.hide();
          }
        },
        error => {
          this.SpinnerService.hide();
          this.isApiError = true;
        });
    }
  }

  openGondieTesterPopup(){
    const dialogRef = this.dialog.open(GondieTesterPopupComponent, {
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
        this.navigateToHome();
    });
  }

  resendOTP(){
    this.isResendOTP = false;
    this.isApiError = false;
    this.isResendOTPFailed = false;
    if(this.subscriberForm.valid){
      this.SpinnerService.show();
      const reqObj = this.createRequestObject();
      this.gondolaService.resendOTPage(reqObj).subscribe(
        resObj => {
          if (resObj !== null) {
            const resObject: any = resObj;
            if(resObject.status === 'success'){
              this.isResendOTP = true;
            } else if(resObject.status === 'failure'){
              this.isResendOTPFailed = true;
            }
            this.SpinnerService.hide();
          }
        },
        error => {
          this.SpinnerService.hide();
          this.isApiError = true;
        });
    }
  } 

  createRequestObject(isOTP ?: boolean): any {
    const requestObject: any = {};
    requestObject.firstname = this.name;
    requestObject.email_address = this.email;
    requestObject.mobilenumber = this.mobileno;
    requestObject.ipaddress = this.gondolaService.ipAddress;
    if(isOTP){
      requestObject.otpnumber = this.otpValue;
    }
    return requestObject;
  }

  allowNumeric(event: any) {
    const pattern = /^[0-9]{1,9}$/;
    if ((event.type === 'keypress' && !pattern.test(event.key)) ||
      (event.type === 'paste' && !pattern.test(event.clipboardData.getData('Text')))) {
      event.preventDefault();
    }
  }

  pageSubmit(type: string){
    if(type === '1'){
      this.isMainPage = false;
      this.isOTPPage = true;
    }
    if(type === '2'){
      this.isMainPage = false;
      this.isOTPPage = false;
      this.isRegisterPage = true;
    }
  }
  navigateToThisIsUs(){
    this.router.navigate(['/ThisIsUs']);
  }
}
