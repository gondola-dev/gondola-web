import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetaInvitationComponent } from './beta-invitation.component';

describe('BetaInvitationComponent', () => {
  let component: BetaInvitationComponent;
  let fixture: ComponentFixture<BetaInvitationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetaInvitationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetaInvitationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
