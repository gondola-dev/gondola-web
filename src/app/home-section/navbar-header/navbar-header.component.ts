import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GondolaService } from 'src/app/services/gondola.service';
import { LoginModelComponent } from '../login-model/login-model.component';

@Component({
  selector: 'app-navbar-header',
  templateUrl: './navbar-header.component.html',
  styleUrls: ['./navbar-header.component.scss']
})
export class NavbarHeaderComponent implements OnInit {
  isCollapsed = false;

  constructor(private router: Router,
    public gondolaService: GondolaService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    window.scroll(0,0);
    
  }
  navigateToThisIsUs(){
    window.scroll(0,0);
    this.router.navigate(['/ThisIsUs']);
  }
  openCalcPopup() {
    if(!this.gondolaService.isNewSubscriber){
      this.router.navigate(['/Calculator']);
    } else {
      this.navigateToInvite();
    }
    window.scroll(0,0);
  }
  navigateToInvite(){
    this.router.navigate(['/BetaInvite']);
    window.scroll(0,0);
  }
  navigateToHome(el ?: any){
    this.gondolaService.age = '';
    this.gondolaService.retirementAge ='';
    this.gondolaService.income = '';
    this.gondolaService.amount = '';
    if(el){
      this.gondolaService.navPageInfo = el;
    }
    this.router.navigate(['']);
  }
  navigateToSection(el: any) {
    document.getElementById(el)?.scrollIntoView({ behavior: 'smooth' });
    //el.scrollIntoView({ behavior: 'smooth' });
  }

  openAdminDashboard(){
    this.router.navigate(['/DashBoardLayout']);
  }
  
  openLoginBox() {
    const dialogRef = this.dialog.open(LoginModelComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        message: 'login',
      },
    });
  }

}
