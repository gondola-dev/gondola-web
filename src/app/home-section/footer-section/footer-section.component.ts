import { Component, OnInit } from '@angular/core';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-footer-section',
  templateUrl: './footer-section.component.html',
  styleUrls: ['./footer-section.component.scss']
})
export class FooterSectionComponent implements OnInit {

  getCurrentYear = (new Date()).getFullYear();

  constructor(
    public gondolaService: GondolaService,) { }

  ngOnInit(): void {
  }

}
