import { Component, OnInit, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CurrencyPipe } from '@angular/common';
import { FormBuilder, Validators, FormGroup, ValidatorFn, AbstractControl, FormControl } from '@angular/forms';
import { GondolaService } from 'src/app/services/gondola.service';
import { HttpClient } from '@angular/common/http';
import { map, Observable, startWith, shareReplay, switchMap } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { NgxSpinnerService } from "ngx-spinner";
import { LoginModelComponent } from '../login-model/login-model.component';
import { StagingUserPopupComponent } from 'src/app/popups/staging-user-popup/staging-user-popup.component';
import { ErrorPopupComponent } from 'src/app/popups/error-popup/error-popup.component';
import { CalcQuotesComponent } from 'src/app/calculator/calc-quotes/calc-quotes.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  isCollapsed = false;

  isAssetCollapsed = false;
  /*
  * To submit the form to stored in api
  */
  isSubmitDone = false;

  /*
  * To store the value of
  */
  amount = '';
  /*
  * To store the value of
  */
  years = '';
  /*
  * To store the value of
  */
  estAnnualIncome = '';
  /*
  * To store the value of
  */
  estRetirementage = '';
  /*
  * To store the value of
  */
  estAssets = '';
  /*
  * To store the value of
  */
  isInvestAst = false;
  /*
  * To store the value of
  */
  isSmoker = '1';
  /*
  * To store the value of
  */
  isWorking = '1';
  /*
  * To store the value of
  */
  age = '';
  /*
  * To store the value of
  */
  statename: any;
  /*
  * To store the value of
  */
  gender = 'Male';

  isApiError = false;
  /**
   * mobile device
   */
  isMobileDevice = false;
  /**
  * mobile device
  */
  isSearchScreenForMobile = false;
  /**
   *  quotes details from group
   */
  quoteDetailsForm!: FormGroup;

  /**
   *   slide per screen
   */
  itemsPerSlide: number = 5;
  /**
   *   hide carosoul indicator
   */
  showIndicator = false;
  /**
   *   interval for slider
   */
  sliderInterval = 5000;
  /**
   *  store image for vendor image slider
   */
  vendorImages: string[] = ["60488.svg", "62200.svg", "64190.svg", "63126.svg", "61301.svg", "15940.svg", "94250.svg", "62383.svg", "62944.svg",
    "63495.svg", "91642.svg", "70939.svg", "64246.svg", "65838.svg", "62057.svg", "65935.svg", "66869.svg", "66915.svg",
    "65528.svg", "66974.svg", "67466.svg", "67644.svg", "61271.svg", "68136.svg", "79227.svg", "60176.svg", "10054.svg",
    "68772.svg", "86231.svg", "69868.svg"];
  /**
   * var
   */
  filteredAmountOptions!: Observable<string[]>;
  filteredYearsOptions!: Observable<string[]>;
  filteredStateOptions!: Observable<any[]>;
  /**
   *  auto complete var
   */
  amountOptions: string[] = ['$25,000', '$50,000', '$75,000', '$100,000', '$125,000', '$150,000', '$200,000', '$250,000', '$300,000', '$350,000', '$400,000', '$450,000', '$500,000', '$550,000', '$600,000', '$650,000', '$700,000', '$750,000', '$800,000', '$850,000', '$900,000', '$950,000', '$1,000,000', '$2,000,000', '$3,000,000', '$4,000,000', '$5,000,000', '$6,000,000', '$7,000,000', '$8,000,000', '$9,000,000', '$10,000,000', '$11,000,000', '$12,000,000', '$13,000,000', '$14,000,000', '$15,000,000', '$16,000,000', '$17,000,000', '$18,000,000', '$19,000,000', '$20,000,000',];
  /**
    *  auto complete var
    */
  yearsOptions: string[] = ['1', '10', '15',
    '20', '25', '30', '35', '40'];
  /**
   * var 
   */
  stateOptions: State[] = [];
  /**
   * var to getStates
   */
  statename$ = this.gondolaService.getStates().pipe(shareReplay());
  /**
   * var 
   */
  statecode = '';

  isUserCall = true;

  recentQuotesList: any = [];


  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private dialog: MatDialog,
    private currencyPipe: CurrencyPipe,
    private SpinnerService: NgxSpinnerService,
    public gondolaService: GondolaService) {
    this.quoteDetailsForm = this.formBuilder.group({
      statename: new FormControl(this.statename, { validators: [this.validateState()], updateOn: 'blur' }),
      isSmoker: new FormControl(this.isSmoker),
      isWorking: new FormControl(this.isWorking),
      gender: new FormControl(this.gender),
      amount: new FormControl(this.amount, { validators: [this.validateAmount()], updateOn: 'change' }),
      age: new FormControl(this.age, { validators: [this.validateAge()], updateOn: 'change' }),
      years: new FormControl(this.years, { validators: [this.validateYears()], updateOn: 'blur' }),
      estAnnualIncome: new FormControl(this.estAnnualIncome, { validators: [this.validateIncome()], updateOn: 'blur' }),
      estRetirementage: new FormControl(this.estRetirementage, { validators: [this.validateRetireAge()], updateOn: 'blur' }),
      estAssets: new FormControl(this.estAssets, { validators: [this.validateEstAsset()], updateOn: 'blur' }),
    });
    //this.stateOptions = this.gondolaService.States;
  }

  ngOnInit(): void {
    this.getUserInfo();
    if (this.gondolaService.navPageInfo) {
      setTimeout(() => {
        console.log('this.gondolaService.navPageInfo  -- ', this.gondolaService.navPageInfo);
        let navInfo = this.gondolaService.navPageInfo;
        this.gondolaService.navPageInfo = '';
        this.navigateToSection(navInfo);
      }, 500);
    }

    this.gondolaService.IsThisUs = false;
    this.gondolaService.IsInvite = false;
    this.gondolaService.isHome = true;
    this.gondolaService.IsCalc = false;

    if (window.screen.width <= 480) {
      this.itemsPerSlide = 3;
      this.isMobileDevice = true;
    } else if (window.screen.width >= 480 && window.screen.width <= 768) {
      this.isMobileDevice = false;
      this.itemsPerSlide = 3;
    } else {
      this.itemsPerSlide = 5;
    }

    this.filteredAmountOptions = this.quoteDetailsForm.get('amount')?.valueChanges.pipe(
      startWith(''),
      map(value => this.amountFilter(value)),
    )!;
    this.filteredYearsOptions = this.quoteDetailsForm.get('years')?.valueChanges.pipe(
      startWith(''),
      map(value => this.yearsFilter(value)),
    )!;
    this.filteredStateOptions = this.quoteDetailsForm.get('statename')?.valueChanges.pipe(
      startWith(''),
      switchMap((name) => {
        return this.statename$.pipe(
          map((stateOptions) => {
            return stateOptions.filter((st: any) =>
              st.name.toLowerCase().includes(name.toLowerCase())
            );
          })
        );
      })
    )!;
  }

  getUserInfo() {
    this.gondolaService.isNewSubscriber = false;
    this.SpinnerService.show();
    this.gondolaService.getIP().subscribe(reqObj => {
      if (reqObj !== null) {
        let ipObj: any = reqObj;
        const ipAddress = ipObj.ip;
        this.gondolaService.ipAddress = ipAddress;
        if (this.gondolaService.isProduction) {
          this.gondolaService.fetchSubscribers(ipAddress).subscribe(
            reqObj => {
              if (reqObj !== null) {
                const requestObj: any = reqObj;
                if (requestObj.pageName === 'invitePage') {
                  this.isUserCall = false;
                  this.SpinnerService.hide();
                  this.gondolaService.isNewSubscriber = true;
                  //this.navigateToInvite();
                } else {
                  this.isUserCall = false;
                  this.SpinnerService.hide();
                  this.gondolaService.firstname = requestObj.firstName;
                  this.gondolaService.emailaddress = requestObj.emailAddress;
                  this.gondolaService.mobilenumber = requestObj.mobileNumber;
                  this.gondolaService.subscriberid = requestObj.subscriberid;
                  const getRecentQuotesList = requestObj.quoteList;
                  if (getRecentQuotesList !== null && getRecentQuotesList) {
                    this.recentQuotesList = this.mapRecentQuotesList(getRecentQuotesList);
                  }
                }
              }
            },
            error => {
              this.SpinnerService.hide();
              this.isUserCall = false;
            })
        } else {
          this.gondolaService.fetchStagingUserInfo(ipAddress).subscribe(
            reqObj => {
              if (reqObj !== null) {
                const requestObj: any = reqObj;
                if (requestObj.pageName === 'popupPage') {
                  this.isUserCall = false;
                  this.SpinnerService.hide();
                  this.OpenStagingInfoPopup();
                } else {
                  this.isUserCall = false;
                  this.SpinnerService.hide();
                  if (requestObj.dataExist) {
                    this.gondolaService.firstname = requestObj.firstName;
                    this.gondolaService.emailaddress = requestObj.emailAddress;
                    this.gondolaService.mobilenumber = requestObj.mobileNumber;
                    this.gondolaService.subscriberid = requestObj.subscriberid;
                  }
                  const getRecentQuotesList = requestObj.quoteList;
                  if (getRecentQuotesList !== null && getRecentQuotesList) {
                    this.recentQuotesList = this.mapRecentQuotesList(getRecentQuotesList);
                  }
                }
              }
            },
            error => {
              this.SpinnerService.hide();
              this.isUserCall = false;
            })
        }

      }
    },
      error => {
      })

  }

  mapRecentQuotesList(ql: any) {
    ql.forEach((element1: any) => {
      this.gondolaService.States.forEach((element: any) => {
        if (element.code === element1.statecode) {
          element1.statename = element.name;
        }
      });
      element1.amount = this.numberWithCommas(element1.amount);
      element1.annualincome = this.numberWithCommas(element1.annualincome);
      element1.smoker = element1.issmoker === "3" ? 'Smoker' : 'Non-Smoker';
      element1.working = element1.isWorking === "1" ? 'Working' : 'Retired';
    });
    return ql;
  }

  getQuotesFromRecentSearch(quoteReq: any) {
    const requestObject: any = {};
    requestObject.amount = this.removeCommas(quoteReq.amount);
    requestObject.years = quoteReq.years;
    requestObject.annualincome = this.removeCommas(quoteReq.annualincome);
    requestObject.expectedretirementage = quoteReq.expectedretirementage;
    requestObject.issmoker = quoteReq.issmoker;
    requestObject.isWorking = quoteReq.isWorking;
    requestObject.age = quoteReq.age;
    requestObject.statename = quoteReq.statename;
    requestObject.statecode = quoteReq.statecode;
    requestObject.gender = quoteReq.gender;
    requestObject.ipaddress = this.gondolaService.ipAddress;
    requestObject.subscriberid = this.gondolaService.subscriberid;
    requestObject.emailaddress = this.gondolaService.emailaddress;
    requestObject.prod = this.gondolaService.isProduction;
    requestObject.estAssets = quoteReq.estAssets;
    this.SpinnerService.show();
    this.gondolaService.createQuotes(requestObject).subscribe(
      resObj => {
        if (resObj !== null) {
          this.setQuoteInfo(resObj, requestObject);
        }
      },
      error => {
        this.SpinnerService.hide();
        this.isApiError = true;
      })
  }

  private amountFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.amountOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  private yearsFilter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.yearsOptions.filter(option => option.toLowerCase().includes(filterValue));
  }

  updateState(event: any) {
    this.statename = event.target.value;
  }

  updateGender(event: any) {
    this.gender = event.target.value;
  }

  updateSmoker(event: any) {
    this.isSmoker = event.target.value;
  }

  updateWorking(event: any) {
    this.isWorking = event.target.value;
    this.estRetirementage = this.estAssets = '';
  }

  displayStateFn(selectedoption: any) {
    return selectedoption ? selectedoption : '';
  }

  navigateToSection(el: any) {
    console.log('el ----- ', el);
    document.getElementById(el)?.scrollIntoView({ behavior: 'smooth' });
    //el.scrollIntoView({ behavior: 'smooth' });
  }

  navigateToInvite() {
    this.router.navigate(['/BetaInvite']);
  }
  navigateToThisIsUs() {
    this.router.navigate(['/ThisIsUs']);
  }

  createNewQuoteObject(): any {
    const requestObject: any = {};
    this.gondolaService.isQuoteAndMaxAmountSame = false;
    const amount = Number(this.removeAmount(this.amount));
    if (this.gondolaService.maxProtectionAmt > 0 && this.gondolaService.maxProtectionAmt < amount) {
      requestObject.amount = this.gondolaService.maxProtectionAmt;
      this.gondolaService.isQuoteAndMaxAmountSame = true;
    } else {
      requestObject.amount = amount;
    }
    this.gondolaService.quoteAmount = requestObject.amount;
    requestObject.years = this.years;
    requestObject.annualincome = this.removeCommas(this.estAnnualIncome);
    requestObject.expectedretirementage = this.estRetirementage;
    requestObject.issmoker = this.isSmoker;
    requestObject.isWorking = this.isWorking;
    requestObject.age = this.age;
    requestObject.statename = this.statename;
    requestObject.statecode = this.statecode;
    requestObject.gender = this.gender;
    requestObject.ipaddress = this.gondolaService.ipAddress;
    requestObject.subscriberid = this.gondolaService.subscriberid;
    requestObject.emailaddress = this.gondolaService.emailaddress;
    requestObject.prod = this.gondolaService.isProduction;
    requestObject.estAssets = this.removeCommas(this.estAssets);
    return requestObject;
  }

  removeDollar(amount: string) {
    return amount ? amount.toString().replace(/\$/g, '') : '';
  }

  removeAmount(amount: string): string {
    let amt = '';
    if (amount && null !== amount) {
      amt = amount.replace(/\$/g, '');
      amt = this.removeCommas(amt);
    }
    return amt;
  }

  enteredQuotes(event: any) {
    if (event.keyCode === 13) {
      this.submitQuotes();
    }
  }

  submitQuotes() {
    this.isApiError = false;
    this.isSubmitDone = true;
    this.isFormValid();
    if (this.quoteDetailsForm.valid) {
      if (!this.gondolaService.isNewSubscriber) {
        this.submitQuotesForSubscriber();
      } else {
        this.navigateToInvite();
      }
    }
  }

  submitQuotesForSubscriber() {
    this.getIncomeTankCalc();
    const QuoteObject = this.createNewQuoteObject();
    this.SpinnerService.show()
    this.gondolaService.createQuotes(QuoteObject).subscribe(
      resObj => {
        if (resObj !== null) {
          const resObject: any = resObj;
          if (resObject.status && resObject.status === 'SUCCESS') {
            this.setQuoteInfo(resObject, QuoteObject);
          } else if (resObject.status === 'FAILURE' && resObject.pageName === 'INVITE_PAGE') {
            this.navigateToInvite();
          }
        }
        this.SpinnerService.hide();
      },
      error => {
        this.SpinnerService.hide();
        this.isApiError = true;
      })
  }

  getIncomeTankCalc() {
    this.gondolaService.getMaxProtectionCalc(Number(this.age), this.isWorking, this.removeCommas(this.estAssets), this.removeCommas(this.estAnnualIncome));
    this.gondolaService.getPotentialCalc(Number(this.age), this.isWorking, this.removeCommas(this.estAssets), this.removeCommas(this.estAnnualIncome), Number(this.estRetirementage));
  }

  isFormValid() {
    this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
    this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
    this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    this.quoteDetailsForm.get('age')?.setValidators(this.validateAge());
    this.quoteDetailsForm.get('age')?.updateValueAndValidity();
    this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
    this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    this.quoteDetailsForm.get('estAnnualIncome')?.setValidators(this.validateIncome());
    this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
    if (this.isWorking === '1') {
      this.quoteDetailsForm.get('estRetirementage')?.setValidators(this.validateRetireAge());
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
      this.quoteDetailsForm.get('estAssets')?.clearValidators();
      this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
    } else if (this.isWorking === '2') {
      this.quoteDetailsForm.get('estAssets')?.setValidators(this.validateEstAsset());
      this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
      this.quoteDetailsForm.get('estRetirementage')?.clearValidators();
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
    }
  }

  popupCheck(): boolean {
    let isValid: boolean = true;
    let errorInfo: any = '';
    if (this.amount) {
      const amountVal = Number(this.removeCommas(this.amount));
      if (amountVal < 100000) {
        isValid = false;
        errorInfo = 'amount';
      }
    }
    if (this.years && Number(this.years) < 10 && Number(this.years) != 1) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'years';
    }
    if (this.age && (Number(this.age) < 18 || Number(this.age) > 80)) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'age';
    }
    if (this.statename) {
      if (this.gondolaService.States !== null) {
        this.gondolaService.States.forEach((e) => {
          if (e.name.toLowerCase() === this.statename.toLowerCase()) {
            this.statecode = e.code;
          }
        });
        if (!this.statecode) {
          isValid = false;
          errorInfo += errorInfo ? '$' : '';
          errorInfo += 'state';
        }
      }
    }
    if (this.estAnnualIncome) {
      const incomeVal = Number(this.removeCommas(this.estAnnualIncome));
      if (incomeVal < 25000) {
        isValid = false;
        errorInfo += errorInfo ? '$' : '';
        errorInfo += 'income';
      }
    }
    if (this.isWorking === '1' && this.estRetirementage && Number(this.estRetirementage) < Number(this.age) &&
      Number(this.estRetirementage) > 120) {
      isValid = false;
      errorInfo += errorInfo ? '$' : '';
      errorInfo += 'estretireage';
    }
    if (this.isWorking === '2' && this.estAssets) {
      const incomeVal = Number(this.removeCommas(this.estAssets));
      if (incomeVal < 100000) {
        isValid = false;
        errorInfo += errorInfo ? '$' : '';
        errorInfo += 'estassets';
      }
    }
    if (!isValid) {
      this.openErrorPopup(errorInfo);
    }
    return isValid;
  }

  validateAmount(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const amountVal = Number(this.removeAmount(this.amount));
      if (!control.value) {
        return { required: true }
      } else if (amountVal < 25000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('amount')?.clearValidators();
        this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateYears(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (Number(this.years) < 10 && Number(this.years) != 1) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('years')?.clearValidators();
        this.quoteDetailsForm.get('years')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateAge(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (!(Number(this.age) >= 18 && Number(this.age) <= 80)) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('age')?.clearValidators();
        this.quoteDetailsForm.get('age')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateState(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      this.setStateCode();
      if (!control.value) {
        return { required: true }
      } else if (!this.statecode) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('statename')?.clearValidators();
        this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateIncome(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const incomeVal = Number(this.removeCommas(this.estAnnualIncome));
      if (!control.value) {
        return { required: true }
      } else if (incomeVal < 25000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estAnnualIncome')?.clearValidators();
        this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateRetireAge(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (Number(this.estRetirementage) < Number(this.age) || Number(this.estRetirementage) > 120) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estRetirementage')?.clearValidators();
        this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
        return null;
      }
    };
  }

  validateEstAsset(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const assetVal = Number(this.removeCommas(this.estAssets));
      if (!control.value) {
        return { required: true }
      } else if (assetVal < 100000) {
        return { invalid: true }
      } else {
        this.quoteDetailsForm.get('estAssets')?.clearValidators();
        this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
        return null;
      }
    };
  }

  setStateCode() {
    this.statecode = '';
    if (this.statename && this.gondolaService.States !== null) {
      this.gondolaService.States.forEach((e) => {
        if (e.name.toLowerCase() === this.statename.toLowerCase()) {
          this.statecode = e.code;
          return true;
        }
      });
    }

  }
  setQuoteInfo(data: any, reqObj: any) {
    if (data !== null) {
      this.gondolaService.productList = data;
      this.gondolaService.requestData = reqObj;
      this.SpinnerService.hide();
      this.router.navigate(['/quotes']);
    }
  }

  openErrorPopup(data: any) {
    const dialogRef = this.dialog.open(ErrorPopupComponent, {
      panelClass: 'custom-dialog-container',
      data: {
        message: data,
      },
    });
  }


  OpenStagingInfoPopup() {
    const dialogRef = this.dialog.open(StagingUserPopupComponent, {
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      this.insertStagingUserInfo();
    });
  }

  insertStagingUserInfo() {
    this.gondolaService.insertStagingUserInfo(this.gondolaService.ipAddress).subscribe(
      reqObj => {
      },
      error => {
      })
  }


  openCalcPopup() {
    this.gondolaService.age = this.age;
    this.gondolaService.retirementAge = this.estRetirementage;
    this.gondolaService.income = this.removeCommas(this.estAnnualIncome);
    this.gondolaService.amount = this.removeAmount(this.amount);

    if (!this.gondolaService.isNewSubscriber) {
      this.router.navigate(['/Calculator']);
    } else {
      this.navigateToInvite();
    }
  }

  openBuildConfidence(pageName: any) {
    this.gondolaService.pageName = pageName;
    this.router.navigate(['/BuildConfidence']);
  }

  gotoQuotes() {
    window.scroll(0, 0);
  }

  allowNumeric(event: any) {
    const pattern = /^[0-9]{1,9}$/;
    if ((event.type === 'keypress' && !pattern.test(event.key)) ||
      (event.type === 'paste' && !pattern.test(event.clipboardData.getData('Text')))) {
      event.preventDefault();
    }
  }

  checkInputValue(event: any): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // convertAmount(event: any){
  //   let convertAmount:any;
  //   convertAmount = this.currencyPipe.transform(event.target.value, '$');
  //   this.amount = convertAmount?.toString();
  // }

  /*convertIncome(event: any){
    let convertIncome:any;
    convertIncome = this.currencyPipe.transform(event.target.value, '$');
    this.estAnnualIncome = convertIncome?.toString();
  }*/

  convertAmount(event: any) {
    this.amount = event.target.value;
    this.amount = this.removeDollar(this.amount);
    this.quoteDetailsForm.get('amount')?.setValue(this.amount);
    if (this.amount !== null && this.amount !== '' && this.amount !== undefined && this.isNumeric(this.amount)) {
      this.amount = this.numberWithCommas(this.amount.replace(/,/g, ''));
      this.amount = '$'.concat(this.amount);
      this.quoteDetailsForm.get('amount')?.setValue(this.amount);
    }
    this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
  }


  convertIncome(event: any) {
    this.estAnnualIncome = event.target.value;
    this.quoteDetailsForm.get('estAnnualIncome')?.setValue(this.estAnnualIncome);
    if (this.estAnnualIncome !== null && this.estAnnualIncome !== '' && this.estAnnualIncome !== undefined && this.isNumeric(this.estAnnualIncome)) {
      this.estAnnualIncome = this.numberWithCommas(this.estAnnualIncome.replace(/,/g, ''));
      this.quoteDetailsForm.get('estAnnualIncome')?.setValue(this.estAnnualIncome);
    }
    this.quoteDetailsForm.get('estAnnualIncome')?.setValidators(this.validateIncome());
    this.quoteDetailsForm.get('estAnnualIncome')?.updateValueAndValidity();
  }

  convertAssets(event: any) {
    this.estAssets = event.target.value;
    this.quoteDetailsForm.get('estAssets')?.setValue(this.estAssets);
    if (this.estAssets !== null && this.estAssets !== '' && this.estAssets !== undefined && this.isNumeric(this.estAssets)) {
      this.estAssets = this.numberWithCommas(this.estAssets.replace(/,/g, ''));
      this.quoteDetailsForm.get('estAssets')?.setValue(this.estAssets);
    }
    this.quoteDetailsForm.get('estAssets')?.setValidators(this.validateAmount());
    this.quoteDetailsForm.get('estAssets')?.updateValueAndValidity();
  }

  validateOnSelect(type: string, event?: any) {
    if (type === 'amount') {
      this.amount = event;
      this.amount = this.removeDollar(this.amount);
      this.quoteDetailsForm.get('amount')?.setValue(this.amount);
      this.quoteDetailsForm.get('amount')?.setValidators(this.validateAmount());
      this.quoteDetailsForm.get('amount')?.updateValueAndValidity();
    }
    else if (type === 'years') {
      this.years = event;
      this.quoteDetailsForm.get('years')?.setValue(this.years);
      this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
      this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    } else if (type === 'state') {
      this.statename = event;
      this.quoteDetailsForm.get('statename')?.setValue(this.statename);
      this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
      this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    }
  }

  validateOnKeyUp(type: string, event?: any) {
    if (type === 'years') {
      this.years = event.target.value;
      this.quoteDetailsForm.get('years')?.setValue(this.years);
      this.quoteDetailsForm.get('years')?.setValidators(this.validateYears());
      this.quoteDetailsForm.get('years')?.updateValueAndValidity();
    } else if (type === 'age') {
      this.age = event.target.value;
      this.quoteDetailsForm.get('age')?.setValue(this.age);
      this.quoteDetailsForm.get('age')?.setValidators(this.validateAge());
      this.quoteDetailsForm.get('age')?.updateValueAndValidity();
    } else if (type === 'state') {
      this.statename = event.target.value;
      this.quoteDetailsForm.get('statename')?.setValue(this.statename);
      this.quoteDetailsForm.get('statename')?.setValidators(this.validateState());
      this.quoteDetailsForm.get('statename')?.updateValueAndValidity();
    } else if (type === 'estretireage') {
      this.estRetirementage = event.target.value;
      this.quoteDetailsForm.get('estRetirementage')?.setValue(this.estRetirementage);
      this.quoteDetailsForm.get('estRetirementage')?.setValidators(this.validateRetireAge());
      this.quoteDetailsForm.get('estRetirementage')?.updateValueAndValidity();
    }
  }

  numberWithCommas(e: any) {
    return e.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

  isNumeric(e: any): boolean {
    if (/^(\d+)?([.]?\d{0,9})?$/.test(this.removeCommas(e))) return true;
    else return false;
  }

  removeCommas(e: any) {
    return e.toString().replace(/,/g, '');
  }

  onFocusAmount(e: any) {
    this.amount = e.target.value;
  }

  onFocusIncome(e: any) {
    this.estAnnualIncome = e.target.value;
  }

  showInvestAst(event: any) {
    event.preventDefault();
    this.isInvestAst = !this.isInvestAst;
  }
  openCalcPop() {
    const dialogRef = this.dialog.open(CalcQuotesComponent, {
      data: {
        message: 'data',
      },
    });

  }
}
class State {
  name = '';
  code = '';
}
