import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { GondolaService } from 'src/app/services/gondola.service';
import { LoginModelComponent } from '../login-model/login-model.component';

@Component({
  selector: 'app-build-confidence',
  templateUrl: './build-confidence.component.html',
  styleUrls: ['./build-confidence.component.scss']
})
export class BuildConfidenceComponent implements OnInit {

  pageName = '';
  isCollapsed = false;
  isreadMore = false;
  dontTrustRM = false;
  myHealthRM = false;
  whoCaresRM = false;
  tooExpRM = false;
  constructor(private router: Router,
    public gondolaService: GondolaService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    window.scroll(0, 0);
    if (this.gondolaService.pageName) {
      this.pageName = this.gondolaService.pageName;
    }
  }

  
  openCalcPopup() {
    if(!this.gondolaService.isNewSubscriber){
    this.router.navigate(['/Calculator']);
    } else {
      this.navigateToInvite();
    }
  }
  
  readMore() {
    this.isreadMore = !this.isreadMore;
  }

  navigateToHome() {
    window.scroll(0, 0);
    this.gondolaService.age = '';
    this.gondolaService.retirementAge = '';
    this.gondolaService.income = '';
    this.gondolaService.amount = '';
    this.router.navigate(['']);
  }

  navigateToInvite() {
    window.scroll(0, 0);
    this.router.navigate(['/BetaInvite']);
  }

  navigateToPage(event: any, name: any) {
    event.preventDefault();
    window.scroll(0, 0);
    this.pageName = name;
  }

  navigateToThisIsUs(){
    window.scroll(0, 0);
    this.router.navigate(['/ThisIsUs']);
  }

  navigateToCalc(event: any) {
    event.preventDefault();
    window.scroll(0, 0);
    this.openCalcPopup();
  }

  contentReadMore(event: any, id: any) {
    event.preventDefault();
    if (id === '12') {
      this.dontTrustRM = !this.dontTrustRM;
    } else if (id === '10') {
      this.myHealthRM = !this.myHealthRM;
    } else if (id === '7') {
      this.whoCaresRM = !this.whoCaresRM;
    } else if (id === '1') {
      this.tooExpRM = !this.tooExpRM;
    }

  }

}
