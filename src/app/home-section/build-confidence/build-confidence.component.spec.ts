import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildConfidenceComponent } from './build-confidence.component';

describe('BuildConfidenceComponent', () => {
  let component: BuildConfidenceComponent;
  let fixture: ComponentFixture<BuildConfidenceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuildConfidenceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildConfidenceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
