import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { NgxOtpInputConfig } from 'ngx-otp-input';
import { NgxSpinnerService } from 'ngx-spinner';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-login-model',
  templateUrl: './login-model.component.html',
  styleUrls: ['./login-model.component.scss']
})
export class LoginModelComponent implements OnInit {

  /**
   * var 
   */
  firstname = '';
  /**
   * var 
   */
  mobilenumber = '';
  /**
   * var 
   */
  emailaddress = '';
  /**
   * var 
   */
  password = '';
  /**
   * var 
   */
  confirmpassword = '';
  /**
   * var 
   */
  subscriberid = 0;
  /**
   * var 
   */
  ipaddress = '';
  /**
   * var 
   */
  resStatus = '';
  /**
   * var 
   */
  resStatusCode = '';
  /**
   * var 
   */
  isreadOnly = false;
  /**
   * var 
   */
  isUserCreated = false;
  /*
  * To submit the form to stored in api
  */
  isSubmitDone = false;
  /**
   * var 
   */
  isApiError = false;
  /**
   * var 
   */
  isApiCallLoading = false;
  /**
   *  login/SignUp details from group
   */
  loginSignUpForm!: FormGroup;
  /**
   * var 
   */
  isSignUp = false;
  /**
   * var 
   */
  isForgetPwd = false;
  /**
   * var 
   */
  otpValue = '';
  /**
   * var 
   */
  isOtpNotFilled = false;
  /**
   * var 
   */
  isOTPVerifyFailed = false;
  /**
   * var 
   */
  isResendOTP = false;
  /**
   * var 
   */
  isResendOTPFailed = false;
  /**
   * var 
   */
  isOTPPage = false;
  /**
   * var 
   */
  isResetPassword = false;
  /**
   * var 
   */
  isPasswordResetSuccess = false;

  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
    public gondolaService: GondolaService,
    private formBuilder: FormBuilder, private router: Router,
    private dialogRef: MatDialogRef<LoginModelComponent>) { dialogRef.disableClose = true; }


  ngOnInit(): void {
    console.log('data -- ', this.data);
    if (this.gondolaService.emailaddress) {
      this.emailaddress = this.gondolaService.emailaddress;
      //this.isreadOnly = true;
    }
    this.firstname = this.gondolaService.firstname ? this.gondolaService.firstname : '';
    this.emailaddress = this.gondolaService.emailaddress ? this.gondolaService.emailaddress : '';
    this.subscriberid = this.gondolaService.subscriberid ? this.gondolaService.subscriberid : 0;
    this.mobilenumber = this.gondolaService.mobilenumber ? this.gondolaService.mobilenumber : '';
    this.loginSignUpForm = this.formBuilder.group({
      firstname: new FormControl(this.firstname, { validators: [Validators.required], updateOn: 'change' }),
      emailaddress: new FormControl(this.emailaddress, [
        Validators.required,
        Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
      mobilenumber: new FormControl(this.mobilenumber),
      password: new FormControl(this.password, { validators: [Validators.required], updateOn: 'change' }),
      confirmpassword: new FormControl(this.confirmpassword, { validators: [Validators.required], updateOn: 'change' }),
    });
    this.loginSignUpForm.get('emailaddress')?.valueChanges.subscribe((event) => {
      this.loginSignUpForm.get('emailaddress')?.setValue(event.toLowerCase(), { emitEvent: false });
    })
  }


  enteredSubmit(event: any) {
    if (event.keyCode === 13) {
      this.loginSubmit();
    }
  }

  loginSubmit() {
    this.isApiError = false;
    this.isSubmitDone = true;
    this.loginSignUpForm.get('emailaddress')?.setValidators([
      Validators.required,
      Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]);
    this.loginSignUpForm.get('emailaddress')?.updateValueAndValidity();
    if (this.isSignUp) {
      this.loginSignUpForm.get('firstname')?.setValidators(this.validateRequired());
      this.loginSignUpForm.get('firstname')?.updateValueAndValidity();
      this.loginSignUpForm.get('password')?.setValidators(this.validateRequired());
      this.loginSignUpForm.get('password')?.updateValueAndValidity();
      this.loginSignUpForm.get('confirmpassword')?.setValidators(this.validateConfirmPassword());
      this.loginSignUpForm.get('confirmpassword')?.updateValueAndValidity();
      this.signUpUserInfo();
    } else if (!this.isSignUp) {
      this.loginSignUpForm.get('firstname')?.clearValidators();
      this.loginSignUpForm.get('firstname')?.updateValueAndValidity();
      this.loginSignUpForm.get('confirmpassword')?.clearValidators();
      this.loginSignUpForm.get('confirmpassword')?.updateValueAndValidity();
      if (!this.isForgetPwd) {
        this.loginSignUpForm.get('password')?.setValidators(this.validateRequired());
        this.loginSignUpForm.get('password')?.updateValueAndValidity();
        this.userLoginInfo();
      } else {
        this.loginSignUpForm.get('password')?.clearValidators();
        this.loginSignUpForm.get('password')?.updateValueAndValidity();
        if(this.isResetPassword){
          this.loginSignUpForm.get('password')?.setValidators(this.validateRequired());
          this.loginSignUpForm.get('password')?.updateValueAndValidity();
          this.loginSignUpForm.get('confirmpassword')?.setValidators(this.validateConfirmPassword());
          this.loginSignUpForm.get('confirmpassword')?.updateValueAndValidity();
        }
        this.forgetPasswordInfo(this.isResetPassword);
      }
    }
  }

  signUpUserInfo() {
    this.resStatus = '';
    this.resStatusCode = '';
    this.isApiError = false;
    if (this.loginSignUpForm.valid) {
      this.isApiCallLoading = true;
      const reqObject = this.createNewRequestObject();
      this.gondolaService.createUserInfo(reqObject).subscribe(
        resObj => {
          if (resObj !== null) {
            let resInfo: any = resObj
            this.resStatus = resInfo.status;
            this.resStatusCode = resInfo.statusCode;
            if (this.resStatusCode === 'OTP_PAGE') {
              this.isOTPPage = true;
            } else if (this.resStatus === 'SUCCESS' && this.resStatusCode === 'USER_CREATED') {
              this.isUserCreated = true;
            }
            this.isApiCallLoading = false;
          }
        },
        error => {
          this.isApiError = true;
          this.isApiCallLoading = false;
        })
    }
  }

  userLoginInfo() {
    this.resStatus = '';
    this.resStatusCode = '';
    this.isApiError = false;
    if (this.loginSignUpForm.valid) {
      this.isApiCallLoading = true;
      const reqObject = this.createNewRequestObject();
      this.gondolaService.loginUserInfo(reqObject).subscribe(
        resObj => {
          if (resObj !== null) {
            let resInfo: any = resObj
            this.resStatus = resInfo.status;
            this.resStatusCode = resInfo.statusCode;
            console.log('resInfo --- ', resInfo);
            if (this.resStatus === 'SUCCESS' && this.resStatusCode === 'EXISTS') {
              this.closeDialog();
              this.gondolaService.firstname = resInfo.firstName;
              this.gondolaService.emailaddress = resInfo.emailAddress;
              this.gondolaService.mobilenumber = resInfo.mobileNumber;
              this.gondolaService.subscriberid = resInfo.subscriberid;
              //this.router.navigate(['/dashboard']);
            }
          }
          this.isApiCallLoading = false;
        },
        error => {
          this.isApiError = true;
          this.isApiCallLoading = false;
        })
    }
  }
  
  forgetPasswordInfo(resetPassword : boolean) {
    this.resStatus = '';
    this.resStatusCode = '';
    this.isApiError = false;
    console.log('form called')
    if (this.loginSignUpForm.valid) {
      this.isApiCallLoading = true;
      const reqObject = this.createNewRequestObject();
      if(!resetPassword){
        this.gondolaService.forgetPassword(reqObject).subscribe(
          resObj => {
            if (resObj !== null) {
              let resInfo: any = resObj;
              this.resStatus = resInfo.status;
              this.resStatusCode = resInfo.statusCode;
              if (this.resStatus === 'SUCCESS') {
                this.isResetPassword = true;
              }
              this.isApiCallLoading = false;
            }
          },
          error => {
            this.isApiError = true;
            this.isApiCallLoading = false;
          })
      } else {
        this.gondolaService.resetPassword(reqObject).subscribe(
          resObj => {
            if (resObj !== null) {
              let resInfo: any = resObj;
              this.resStatus = resInfo.status;
              this.resStatusCode = resInfo.statusCode;
              if (this.resStatus === 'SUCCESS') {
                this.isPasswordResetSuccess = true;
              }
              this.isApiCallLoading = false;
            }
          },
          error => {
            this.isApiError = true;
            this.isApiCallLoading = false;
          })
      }
    }
  }

  createNewRequestObject(isOTP?: boolean) {
    const requestObject: any = {};
    if (this.isSignUp) {
      requestObject.firstname = this.firstname;
      requestObject.mobilenumber = this.mobilenumber;
      requestObject.confirmpassword = this.confirmpassword;
    }
    requestObject.emailaddress = this.emailaddress;
    if (!this.isForgetPwd || this.isResetPassword) {
      requestObject.password = this.password;
      if(this.isResetPassword){
        requestObject.confirmpassword = this.confirmpassword;
      }
    }
    requestObject.subscriberid = this.subscriberid;
    requestObject.ipaddress = this.gondolaService.ipAddress;
    if (isOTP) {
      requestObject.otpnumber = this.otpValue;
    }
    return requestObject;
  }

  validateRequired(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else {
        return null;
      }
    };
  }
  validateConfirmPassword(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      if (!control.value) {
        return { required: true }
      } else if (this.password && this.password !== this.confirmpassword) {
        return { mismatch: true }
      } else {
        return null;
      }
    };
  }
  closeDialog() {
    this.dialogRef.close();
  }

  moveToSignInUp(event: any, flow: any) {
    event.preventDefault();
    this.clearValues();
    this.isForgetPwd = false;
    if (flow === 'signin') {
      this.isSignUp = false;
    } else if (flow === 'signup') {
      this.isSignUp = true;
    }
    this.loginSignUpForm.get('firstname')?.setValidators([Validators.required]);
    this.loginSignUpForm.get('firstname')?.updateValueAndValidity();
    this.loginSignUpForm.get('password')?.setValidators([Validators.required]);
    this.loginSignUpForm.get('password')?.updateValueAndValidity();
    this.loginSignUpForm.get('confirmpassword')?.setValidators([Validators.required]);
    this.loginSignUpForm.get('confirmpassword')?.updateValueAndValidity();
  }

  moveToForgetPwd(event: any) {
    event.preventDefault();
    this.clearValues();
    this.isForgetPwd = true;
  }

  clearValues() {
    this.firstname = '';
    this.mobilenumber = '';
    this.password = '';
    this.confirmpassword = '';
    this.isSubmitDone = false;
    this.resStatus = '';
    this.resStatusCode = '';
    this.isOTPPage = false;
    this.isUserCreated = false;
    this.isApiCallLoading = false;
    this.isResetPassword = false;
    this.isPasswordResetSuccess = false;
    /*this.loginSignUpForm.get('firstname')?.clearValidators();
    this.loginSignUpForm.get('firstname')?.updateValueAndValidity();
    this.loginSignUpForm.get('confirmpassword')?.clearValidators();
    this.loginSignUpForm.get('confirmpassword')?.updateValueAndValidity();
    this.loginSignUpForm.get('emailaddress')?.clearValidators();
    this.loginSignUpForm.get('emailaddress')?.updateValueAndValidity();
    this.loginSignUpForm.get('password')?.clearValidators();
    this.loginSignUpForm.get('password')?.updateValueAndValidity();*/
  }

  /* OTP Page Flow*/
  otpInputConfig: NgxOtpInputConfig = {
    otpLength: 6,
    autofocus: true,
    classList: {
      inputBox: 'my-super-box-class',
      input: 'my-super-class',
      inputFilled: 'my-super-filled-class',
      inputDisabled: 'my-super-disable-class',
      inputSuccess: 'my-super-success-class',
      inputError: 'my-super-error-class',
    },
  };

  handeOtpChange(value: string[]): void {
    this.otpValue = '';
    value.forEach(val => {
      if (val) {
        this.otpValue += val;
      }
    })
  }

  /* handleFillEvent(value: string): void {
     this.otpValue = value;
   }*/

  validateOTP() {
    this.isOtpNotFilled = false;
    this.isOTPVerifyFailed = false;
    if (this.otpValue && this.otpValue.length === 6) {
      const reqObj = this.createNewRequestObject(true);
      this.isApiCallLoading = true;
      this.gondolaService.validateSignInOTP(reqObj).subscribe(
        resObj => {
          if (resObj !== null) {
            let resInfo: any = resObj
            if (resInfo.status === 'SUCCESS' && resInfo.statusCode === 'USER_CREATED') {
              this.isUserCreated = true;
            } else if (resInfo.status === 'FAILURE') {
              this.isOTPVerifyFailed = true;
              this.isApiCallLoading = false;
            }
          }
        },
        error => {
          this.isApiCallLoading = false;
          this.isApiError = true;
        });
    } else {
      this.isOtpNotFilled = true;
    }
  }

  navigateToHome() {
    window.scroll(0, 0);
    this.router.navigate(['']);
  }


  resendOTP() {
    this.isResendOTP = false;
    this.isApiError = false;
    this.isResendOTPFailed = false;
    if (this.loginSignUpForm.valid) {
      this.isApiCallLoading = true;
      const reqObj = this.createNewRequestObject();
      this.gondolaService.resendSignInOTP(reqObj).subscribe(
        resObj => {
          if (resObj !== null) {
            const resObject: any = resObj;
            if (resObject.status === 'success') {
              this.isResendOTP = true;
            } else if (resObject.status === 'failure') {
              this.isResendOTPFailed = true;
            }
            this.isApiCallLoading = false;
          }
        },
        error => {
          this.isApiCallLoading = false;
          this.isApiError = true;
        });
    }
  }
}
