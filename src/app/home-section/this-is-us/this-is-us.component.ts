import { Component, OnInit } from '@angular/core';
import { LoginModelComponent } from '../login-model/login-model.component';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-this-is-us',
  templateUrl: './this-is-us.component.html',
  styleUrls: ['./this-is-us.component.scss']
})
export class ThisIsUsComponent implements OnInit {

  isCollapsed = false;
  isreadMore = false;
  constructor(private router: Router,
    public gondolaService: GondolaService,
    private dialog: MatDialog) { }

  ngOnInit(): void {
    this.gondolaService.IsThisUs = true;
    this.gondolaService.IsInvite = false;
    this.gondolaService.isHome = false;
    this.gondolaService.IsCalc = false;  
    window.scroll(0, 0);
  }
  
  navigateToHome(){
    this.gondolaService.age = '';
    this.gondolaService.retirementAge ='';
    this.gondolaService.income = '';
    this.gondolaService.amount = '';
    this.router.navigate(['']);
  }

  navigateToInvite(){
    this.router.navigate(['/BetaInvite']);
  }

  readMore(){
    this.isreadMore = !this.isreadMore;
    if(this.isreadMore){
      document.getElementById("dvscrollReadMore")?.scrollIntoView({ behavior: 'smooth' });
    } else {
      window.scroll(0,0);
    }
  }

}
