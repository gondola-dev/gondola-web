import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ThisIsUsComponent } from './this-is-us.component';

describe('ThisIsUsComponent', () => {
  let component: ThisIsUsComponent;
  let fixture: ComponentFixture<ThisIsUsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ThisIsUsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ThisIsUsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
