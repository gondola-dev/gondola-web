import { style } from '@angular/animations';

import { Component, OnInit, Inject } from '@angular/core';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';


@Component({

  selector: 'app-beta-launch',

  templateUrl: './beta-launch.component.html',

  styleUrls: ['./beta-launch.component.scss']

})

export class BetaLaunchComponent implements OnInit {


  isMobileDevice = false;


  constructor(@Inject(MAT_DIALOG_DATA) private data: any,

  private dialogRef: MatDialogRef<BetaLaunchComponent>) {dialogRef.disableClose = true; }


  ngOnInit(): void {

    this.isMobileDevice = this.data.isMobileDevice;

    console.log('data -- ',this.isMobileDevice);

  }
  closeDialog(){
    this.dialogRef.close();
  }

}