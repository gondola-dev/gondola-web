import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class GondolaService {
  productList: any;

  requestData: any;

  States: State[]= [];

  insurersList: any[] = [];

  ipAddress = '';

  age = '';

  retirementAge = '';

  income = '';

  amount = '';

  pageName = '';

  emailaddress = '';

  firstname = '';

  mobilenumber = '';

  subscriberid = 0;

  recentQuotesList : any = [];

  navPageInfo: any = '';

  private baseUrl;

  isProduction = false;

  appURL = '';

  IsInvite = false;
  
  IsThisUs = false;

  IsCalc = false;

  isHome= true;

  maxPotentialAmt: number = 0;

  maxProtectionAmt: number = 0;

  quoteAmount: number = 0;

  isQuoteAndMaxAmountSame = false;
  
  hideSideNav: boolean = false;

  isNewSubscriber: boolean = false;

  constructor(private http: HttpClient,
  ) {
    if(environment.apiKey === 'prodKey'){
      this.isProduction = true;
      this.baseUrl = 'https://devapi.gondola.life:8080/gondola/api/v1/';
      this.appURL = 'https://gondola.life';
    } else if(environment.apiKey === 'stagingKey'){
      this.baseUrl = 'https://stagingapi.gondola.life:8080/gondola/api/v1/';
      this.appURL = 'https://staging.gondola.life';
    } else {
      this.baseUrl = 'http://localhost:8080/gondola/api/v1/';
      this.appURL = 'http://localhost:4200';
      this.isProduction = true;
    }
    //this.getIpAddress();
    this.getStatesList();
    this.getInsurersList();
  }

  /*getIpAddress(): string{
    if(!this.ipAddress){
    this.getIP().subscribe(reqObj => {
      if (reqObj !== null) {
        let ipObj:any = reqObj;
        this.ipAddress = ipObj.ip;
        return this.ipAddress;
      }
      },
      error => {
      })
    }
    return this.ipAddress;
  }*/

  toggleSideNav(): void {
    this.hideSideNav = !this.hideSideNav;
  }

  getStatesList():State[]{
    if(this.States.length === 0){
      this.getStates().subscribe(
        reqObj => {
          if (reqObj !== null) {
            let stateList: any = reqObj;
            stateList.forEach((element: any) => {
              const state = new State();
              state.code = element.code;
              state.name = element.name;
              this.States.push(state);
            });
          }
        },
        error => {
        })
      }
    return this.States;
  }

  getInsurersList():any[]{
    if(this.insurersList.length === 0){
      this.fetchInsurers().subscribe(
        reqObj => {
          if (reqObj !== null) {
            let insurers: any = reqObj;
            /*let firstIns = {id: 0, 
              insurername: 'All', 
              insurershortname: 'All', 
              naiccode: 0}
              this.insurersList.push(firstIns);*/
            insurers.forEach((ele:any)=>{
              this.insurersList.push(ele)
            })
          }
        },
        error => {
        })
    }
   return this.insurersList;
  }

  getUserInfo(ipaddress :any ){
    this.fetchSubscribers(
      ipaddress).subscribe(
      reqObj => {
        if (reqObj !== null) {
          const requestObj: any = reqObj;
          if(requestObj.pageName === 'invitePage'){
            //this.navigateToInvite();
          } else {
            this.firstname = requestObj.firstName;
            this.emailaddress = requestObj.emailAddress;
            this.mobilenumber = requestObj.mobileNumber;
            this.subscriberid = requestObj.subscriberid;
            this.recentQuotesList = requestObj.quoteList;
          }
        }
      },
      error => {
      })
  }
  getIP(): Observable<object> {
    return this.http.get('https://jsonip.com/');
}
  
  createQuotes(quotes: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'quotes', quotes);
  }

  createSubscriberInfo(subscriberInfo: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'createsubscriber', subscriberInfo);
  }

  
  validateOTP(reqObj: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'validateOTP', reqObj);
  }

  resendOTPage(subscriberInfo: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'resendOTP', subscriberInfo);
  }
  
  getUserList(): Observable<object> {
    return this.http.get(`${this.baseUrl}` + 'userList');
  }

  createUserInfo(reqInfo: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'createuserinfo', reqInfo);
  }
  
  loginUserInfo(reqInfo: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'getuserlist', reqInfo);
  }

  forgetPassword(reqObj: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'forgetPassword', reqObj);
  }

  resetPassword(reqObj: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'resetPassword', reqObj);
  }
  
  validateSignInOTP(reqObj: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'validateSignInOTP', reqObj);
  }

  resendSignInOTP(subscriberInfo: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'resendSignInOTP', subscriberInfo);
  }

  insertUserInfo(reqObj: object): Observable<object> {
    return this.http.post(`${this.baseUrl}` + 'insertuserlist', reqObj);
  }

  fetchInsurers(): Observable<object>{
    return this.http.get(`${this.baseUrl}` + 'insurerlist');
  }

  
  fetchSubscribers(ipaddress: any): Observable<object>{
    return this.http.get(`${this.baseUrl}` + 'fetchUserInfo?ipaddress='+ipaddress);
  }

  
  fetchStagingUserInfo(ipaddress: any): Observable<object>{
    return this.http.get(`${this.baseUrl}` + 'fetchStagingInfo?ipaddress='+ipaddress);
  }
  
  insertStagingUserInfo(ipaddress: any): Observable<object> {
    return this.http.get(`${this.baseUrl}` + 'insertStagingUser?ipaddress='+ipaddress);
  }

  getStates() {
    return this.http.get<any>(`${this.baseUrl}` + 'states');
  }

  getPotentialCalc(age: any, working:string, estAsset: any, Annualincome: any, retirementage:any) {
		let potential = 0;
    if (working === "1" && age > 0 && retirementage >= age) {
      const calcage = retirementage - age;
      let income = potential = Annualincome;
      let annualincome = 0;
      for (let i = 0; i < calcage; i++) {
        if (i != 0) {
          annualincome = Number(Number(income) + Number(Number(potential) * 0.03));
          potential = Number(Number(potential) + Number(annualincome));
        }
      }
    } else if (working === "2" && estAsset > 0) {
      potential = estAsset;
    }
		this.maxPotentialAmt = Math.round(potential);
	}

	getMaxProtectionCalc(age: any, working:string, estAsset: any,Annualincome:any) {
		let maxProtect = 0;
    if (working === "2" && estAsset > 0) {
      maxProtect = estAsset / 2;
    } else if (age > 0 && working === "1") {
      if (age >= 18 && age <= 40) {
        maxProtect = 30 * Annualincome;
      } else if (age >= 41 && age <= 50) {
        maxProtect = 25 * Annualincome;
      } else if (age >= 51 && age <= 55) {
        maxProtect = 20 * Annualincome;
      } else if (age >= 56 && age <= 65) {
        maxProtect = 15 * Annualincome;
      } else if (age >= 66 && age <= 70) {
        maxProtect = 10 * Annualincome;
      } else if (age >= 71 && age <= 74) {
        maxProtect = 5 * Annualincome;
      }
    }
		this.maxProtectionAmt = Math.round(maxProtect);
	}
}
class State {
  name = '';
  code = '';
}

