import { TestBed } from '@angular/core/testing';

import { GondolaService } from './gondola.service';

describe('GondolaService', () => {
  let service: GondolaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GondolaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
