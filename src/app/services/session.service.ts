import { Injectable } from "@angular/core";

const GONDOLA_TOKEN = "gondola_token";

@Injectable({
  providedIn: "root",
})
export class SessionService {
  constructor() {
  }

  setUser(userInfo: any): void {
    localStorage.setItem(GONDOLA_TOKEN, JSON.stringify(userInfo));
  }

  setToken(token: string): void {
    localStorage.setItem(GONDOLA_TOKEN, token);
  }

  getToken(): string | null | undefined {
    return localStorage.getItem(GONDOLA_TOKEN);
  }

  deleteUser(): void {
    sessionStorage.clear();
    localStorage.clear();
  }
}
