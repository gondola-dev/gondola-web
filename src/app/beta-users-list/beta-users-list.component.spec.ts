import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BetaUsersListComponent } from './beta-users-list.component';

describe('BetaUsersListComponent', () => {
  let component: BetaUsersListComponent;
  let fixture: ComponentFixture<BetaUsersListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BetaUsersListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BetaUsersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
