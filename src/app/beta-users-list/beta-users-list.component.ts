import { Component, OnInit } from '@angular/core';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-beta-users-list',
  templateUrl: './beta-users-list.component.html',
  styleUrls: ['./beta-users-list.component.scss']
})
export class BetaUsersListComponent implements OnInit {
  sideNavStatus: boolean = false;

  betaUserList: any = [];

  constructor(private gondolaService: GondolaService) { }

  ngOnInit(): void {
    this.getBetaUsers();

  }
  
  getBetaUsers() {
    this.gondolaService.getUserList()
      .subscribe(res => {
        console.log(res);
        this.betaUserList = res;
        console.log(this.betaUserList);
      });
  }

}