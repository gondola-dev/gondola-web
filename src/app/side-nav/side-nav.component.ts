import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';


@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
  @Input() sideNavStatus: boolean = false;
  list = [
    {
      number: '1',
      name: 'Dashboard',
      icon:  'bi bi-columns',

    },
    {
      number: '2',
      name: 'Search Quote',
      icon:  'bi bi-search',
    },
    {
      number: '3',
      name: 'Beta Users',
      icon:  'bi bi-person-circle',
    },
    {
      number: '4',
      name: 'Quote List',
      icon:  'bi bi-card-checklist',

    },
    {
      number: '5',
      name: 'Carrier Info',
      icon:  'bi bi-building',
    },
    {
      number: '6',
      name: 'States Info',
      icon:  'bi bi-flag',
    },
    {
      number: '7',
      name: 'Users Info',
      icon:  'bi bi-people-fill',

    },
    {
      number: '5',
      name: 'Admin List',
      icon:  'bi bi-person-square',
    },
    {
      number: '7',
      name: 'Settings',
      icon:  'bi bi-gear-fill',
    } 
  ]

  constructor( private router: Router) { }

  ngOnInit(): void {
    
  }
  openUsersList() {
    this.router.navigate(['/BetaUsersList']);
}

}
