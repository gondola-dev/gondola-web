import { NgModule } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from "ngx-spinner";
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { RouterModule } from '@angular/router';
import { MaterialExampleModule } from 'src/material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPrintModule } from 'ngx-print'; 
import { NgxOtpInputModule } from "ngx-otp-input";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home-section/home/home.component';
import { GetQuotesComponent } from './productinfo/get-quotes/get-quotes.component';
import { LoginModelComponent } from './home-section/login-model/login-model.component';
import { FooterSectionComponent } from './home-section/footer-section/footer-section.component';
import { ApplyQuotesComponent } from './productinfo/apply-quotes/apply-quotes.component';
import { MatIconModule } from "@angular/material/icon";
import { BetaLaunchComponent } from './home-section/beta-launch/beta-launch.component';
import { ProductDetailsComponent } from './productinfo/product-details/product-details.component';
import { ErrorPopupComponent } from './popups/error-popup/error-popup.component';
import { CalcQuotesComponent } from './calculator/calc-quotes/calc-quotes.component';
import { BetaInvitationComponent } from './home-section/beta-invitation/beta-invitation.component';
import { RemovedPlansComponent } from './productinfo/removed-plans/removed-plans.component';
import { CalcQuotesNewComponent } from './calculator/calc-quotes-new/calc-quotes-new.component';
import { ProceedsLastComponent } from './calculator/proceeds-last/proceeds-last.component';
import { MinimumCoverageComponent } from './calculator/minimum-coverage/minimum-coverage.component';
import { NeedvswantComponent } from './calculator/needvswant/needvswant.component';
import { PrintQuotesComponent } from './productinfo/print-quotes/print-quotes.component';
import { CalcDocumentProofComponent } from './calculator/calc-document-proof/calc-document-proof.component';
import { ThisIsUsComponent } from './home-section/this-is-us/this-is-us.component';
import { BuildConfidenceComponent } from './home-section/build-confidence/build-confidence.component';
import { StagingUserPopupComponent } from './popups/staging-user-popup/staging-user-popup.component';
import { GondieTesterPopupComponent } from './popups/gondie-tester-popup/gondie-tester-popup.component';
import { MobileMaskDirective } from './directives/mobile-mask.directive';
import { NavbarHeaderComponent } from './home-section/navbar-header/navbar-header.component';
import { AddonsPopupComponent } from './popups/addons-popup/addons-popup.component';
import { UserDashboardComponent } from './dashboard/users/user-dashboard/user-dashboard.component'; 
import { TopNavComponent } from './dashboard/dashboard-layout/top-nav/top-nav.component';
import { UserSideNavComponent } from './dashboard/users/user-side-nav/user-side-nav.component';
import { DashboardLayoutComponent } from './dashboard/dashboard-layout/dashboard-layout.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { BetaUsersListComponent } from './beta-users-list/beta-users-list.component';

@NgModule({
  exports: [
      MobileMaskDirective,
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    GetQuotesComponent,
    LoginModelComponent,
    FooterSectionComponent,
    ApplyQuotesComponent,
    BetaLaunchComponent,
    ProductDetailsComponent,
    ErrorPopupComponent,
    CalcQuotesComponent,
    BetaInvitationComponent,
    RemovedPlansComponent,
    CalcQuotesNewComponent,
    ProceedsLastComponent,
    MinimumCoverageComponent,
    NeedvswantComponent,
    PrintQuotesComponent,
    CalcDocumentProofComponent,
    ThisIsUsComponent,
    BuildConfidenceComponent,
    StagingUserPopupComponent,
    GondieTesterPopupComponent,
    MobileMaskDirective,
    NavbarHeaderComponent,
    AddonsPopupComponent,
    UserDashboardComponent,
    TopNavComponent,
    UserSideNavComponent,
    DashboardLayoutComponent,
    AdminDashboardComponent,
    SideNavComponent,
    BetaUsersListComponent,
    
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    NgxPrintModule,
    NgxOtpInputModule,
    CarouselModule,
    MatIconModule,
    MaterialExampleModule,
    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent
      },
      {
        path: 'quotes',
        component: GetQuotesComponent
      },
      {
        path: 'protect-now',
        component: ApplyQuotesComponent
      },
      {
        path: 'BetaInvite',
        component: BetaInvitationComponent
      },
      {
        path: 'Calcquotes',
        component: CalcQuotesComponent
      },
      {
        path: 'Calculator',
        component: CalcQuotesNewComponent
      },
      {
        path: 'quotesnew',
        component: PrintQuotesComponent
      },
      {
        path: 'ThisIsUs',
        component: ThisIsUsComponent
      },
      {
        path: 'BuildConfidence',
        component: BuildConfidenceComponent
      },
      {
        path: 'dashboard',
        component: UserDashboardComponent
      },
      {
        path: 'AdminDashboard',
        component: AdminDashboardComponent
      },
      {
        path: 'BetaUsersList',
        component: BetaUsersListComponent
      },
      {
        path: 'UserSideNav',
        component: UserSideNavComponent
      },
      {
        path: 'SideNav',
        component: SideNavComponent
      },
      {
        path: 'DashBoardLayout',
        component: DashboardLayoutComponent
      }
    ])
  ],
  providers: [CurrencyPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
