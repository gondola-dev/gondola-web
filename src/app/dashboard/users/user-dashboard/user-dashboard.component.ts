import { Component, OnInit } from '@angular/core';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.scss']
})
export class UserDashboardComponent implements OnInit {

  constructor(
    public gondolaService: GondolaService) { }

  ngOnInit(): void {
  }

}
