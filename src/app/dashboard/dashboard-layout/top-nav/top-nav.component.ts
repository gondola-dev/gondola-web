import { Component, OnInit } from '@angular/core';
import { GondolaService } from 'src/app/services/gondola.service';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss']
})
export class TopNavComponent implements OnInit {

  constructor(public gondolaService: GondolaService) { }

  ngOnInit(): void {
  }

}
