import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "filter",
})
export class FilterPipe implements PipeTransform {
  transform(arr: any, search: string, fields: any): any {
    if (!Array.isArray(arr) || !arr.length) {
      return arr;
    }
    if (!search || !search.trim()) {
      return arr;
    }
    return arr.filter((obj: any) => {
      return fields.find((field: any) => {
        return obj[field]?.toLowerCase().includes(search.toLowerCase());
      });
    });
  }
}
