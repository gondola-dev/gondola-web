import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-addons-popup',
  templateUrl: './addons-popup.component.html',
  styleUrls: ['./addons-popup.component.scss']
})
export class AddonsPopupComponent implements OnInit {
  message = '';
  constructor() { }

  ngOnInit(): void {
  }

}
