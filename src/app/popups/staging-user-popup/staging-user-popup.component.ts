import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-staging-user-popup',
  templateUrl: './staging-user-popup.component.html',
  styleUrls: ['./staging-user-popup.component.scss']
})
export class StagingUserPopupComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<StagingUserPopupComponent>) {dialogRef.disableClose = true; }

  ngOnInit(): void {
  }

  
  closeDialog(){
    this.dialogRef.close();
  }

}
