import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StagingUserPopupComponent } from './staging-user-popup.component';

describe('StagingUserPopupComponent', () => {
  let component: StagingUserPopupComponent;
  let fixture: ComponentFixture<StagingUserPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StagingUserPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(StagingUserPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
