import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GondieTesterPopupComponent } from './gondie-tester-popup.component';

describe('GondieTesterPopupComponent', () => {
  let component: GondieTesterPopupComponent;
  let fixture: ComponentFixture<GondieTesterPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GondieTesterPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GondieTesterPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
