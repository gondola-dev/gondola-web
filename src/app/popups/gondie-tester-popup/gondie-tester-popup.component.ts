import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-gondie-tester-popup',
  templateUrl: './gondie-tester-popup.component.html',
  styleUrls: ['./gondie-tester-popup.component.scss']
})
export class GondieTesterPopupComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<GondieTesterPopupComponent>) {dialogRef.disableClose = true; }

  ngOnInit(): void {
    window.scroll(0,0);
  }

  
  closeDialog(){
    this.dialogRef.close();
  }

}
