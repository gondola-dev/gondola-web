import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-error-popup',
  templateUrl: './error-popup.component.html',
  styleUrls: ['./error-popup.component.scss']
})
export class ErrorPopupComponent implements OnInit {

  message = '';
  errorList: any = [];
  
  constructor(@Inject(MAT_DIALOG_DATA) private data: any,
  private dialogRef: MatDialogRef<ErrorPopupComponent>) {dialogRef.disableClose = true; }


  ngOnInit(): void {
    this.message = this.data.message;
    console.log('data -- ',this.data);
    
    let myArray = this.message.split("$");
    this.errorList = myArray;
    console.log('myArray -- ',myArray);
    
    if(myArray.length > 0){
      myArray.forEach((e)=>{
        console.log(e);
      })
    }
  }

  closeDialog(){
    this.dialogRef.close();
  }

}
